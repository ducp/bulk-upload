﻿using BulkUpload.Helpers;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace BulkUpload
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var templatesFolder = ConfigurationProvider.GetValue<string>(SupportedConfiguration.TemplatesLocation);
            if (!Directory.Exists(templatesFolder))
            {
                Directory.CreateDirectory(templatesFolder);
            }

            // create temp folder for saving temp files
            var tempFolder = ConfigurationProvider.GetValue<string>(SupportedConfiguration.TempLocation);
            if (!Directory.Exists(tempFolder))
            {
                Directory.CreateDirectory(tempFolder);
            }

            // create temp folder for saving temp files
            var importFilesLocation = ConfigurationProvider.GetValue<string>(SupportedConfiguration.ImportFilesLocation);
            if (!Directory.Exists(importFilesLocation))
            {
                Directory.CreateDirectory(importFilesLocation);
            }

            var breezePropertiesFileLocation = ConfigurationProvider.GetValue<string>(SupportedConfiguration.BreezePropertiesFileLocation);
            if (!Directory.Exists(breezePropertiesFileLocation))
            {
                Directory.CreateDirectory(breezePropertiesFileLocation);
            }

            var soapTemplatesLocation = ConfigurationProvider.GetValue<string>(SupportedConfiguration.SOAPTemplatesLocation);
            if (!Directory.Exists(soapTemplatesLocation))
            {
                Directory.CreateDirectory(soapTemplatesLocation);
            }

            var exportedXLSXRegLoanDataFilesLocation = ConfigurationProvider.GetValue<string>(SupportedConfiguration.ExportedXLSXRegLoanDataFilesLocation);
            if (!Directory.Exists(exportedXLSXRegLoanDataFilesLocation))
            {
                Directory.CreateDirectory(exportedXLSXRegLoanDataFilesLocation);
            }

            var exportedErrorLoansFilesLocation = ConfigurationProvider.GetValue<string>(SupportedConfiguration.ExportedErrorLoansFilesLocation);
            if (!Directory.Exists(exportedErrorLoansFilesLocation))
            {
                Directory.CreateDirectory(exportedErrorLoansFilesLocation);
            }

            var previewLoansFilesLocation = ConfigurationProvider.GetValue<string>(SupportedConfiguration.PreviewLoansFilesLocation);
            if (!Directory.Exists(previewLoansFilesLocation))
            {
                Directory.CreateDirectory(previewLoansFilesLocation);
            }

        }

        //protected void Application_AuthenticateRequest(object sender, EventArgs e)
        //{
        //    var cookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
        //    if (cookie != null)
        //    {
        //        FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);

        //        var identity = new GenericIdentity(ticket.Name);
        //        var data = JsonConvert.DeserializeObject<BulkUpload.Models.BreezeUserData>(ticket.UserData);
        //        var roles = data.Roles;
        //        var principle = new GenericPrincipal(identity, roles.ToArray());

        //        HttpContext.Current.User = principle;
        //        Thread.CurrentPrincipal = principle;
        //    }
        //}
    }
}
