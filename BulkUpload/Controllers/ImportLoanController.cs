﻿
using BulkUpload.Models;
using BulkUpload.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BulkUpload.Controllers
{

    public class ImportLoanController : BaseController
    {
        private readonly ITemplateService _templateService;
        private readonly ILoanService _loanService;

        public ImportLoanController(ITemplateService templateService, ILoanService loanService)
        {
            _templateService = templateService;
            _loanService = loanService;
        }

        // GET: ImportLoan
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LoadPreviewContent(int clientId, int templateId)
        {
            var template = await _templateService.GetById(templateId);
            if (template == null)
            {
                throw new Exception($"no template with Id: {templateId}");
            }

            // only accept one file
            HttpFileCollectionBase files = Request.Files;
            HttpPostedFileBase file = files[0];

            var data = await _loanService.ExtractClientLoansFromPostedFile(clientId, templateId);

            return PartialView("_Preview", data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ImportLoans(int importSessionId = 0, int clientId = 0, int templateId = 0, string loansString = "")
        {
            // only accept one file
            HttpFileCollectionBase files = Request.Files;
            HttpPostedFileBase file = files[0];

            var importResult = await _loanService.ImportLoans(importSessionId, clientId, templateId, loansString, file);
            return PartialView("_CommitResult", importResult);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> GenerateErrorLoansFile(string loansString)
        {
            var errorLoans = JsonConvert.DeserializeObject<List<ClientLoanDataModel>>(loansString);
            var file = await _loanService.GenerateErrorLoansFile(errorLoans);
            return Json(file, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DownloadErrorLoansFile(string filename)
        {
            string fullPath = _loanService.GetErrorLoansFileLocation(filename);
            var name = System.IO.Path.GetFileName(filename);
            return File(fullPath, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", name);
        }

    }

}