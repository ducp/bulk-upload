﻿using BulkUpload.Entities;
using BulkUpload.Helpers;
using BulkUpload.Models;
using BulkUpload.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace BulkUpload.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserService _userService;

        public HomeController(IUserService userService)
        {
            _userService = userService;
        }

        public ActionResult Login(string returnUrl = "")
        {
            return View(new LoginModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        public ActionResult Login(LoginModel loginModel)
        {
            if (!ModelState.IsValid)
            {
                return View(loginModel);
            }

            bool loginResult = _userService.Login(loginModel.Username, loginModel.Password);
            if (!loginResult)
            {
                ModelState.AddModelError("FailedToAuthenticateInEpicAppLogin", "Failed to authenticate user.");
                return View(loginModel);
            }

            this.OnUserAuthenticated(loginModel.Username);

            if (!string.IsNullOrWhiteSpace(loginModel.ReturnUrl))
            {
                return Redirect(loginModel.ReturnUrl);
            }

            return RedirectToAction("Index", "Template");
        }

        private void OnUserAuthenticated(string username)
        {
            // create user claims
            BreezeUserData userData = new BreezeUserData
            {
                Username = username,
                Clients = new List<Client>{
                    new Client { ClientId = 169023, Name = "ML Mortgage Corp." },
                    new Client { ClientId = 169981, Name = "Premier Mortgage Resources, L.L.C. " }
                },
                Roles = new List<string> { "Secondary", "Admin" }
            };
            var data = JsonConvert.SerializeObject(userData);

            // set expire time for cookie
            var cookieExpiredIn = ConfigurationProvider.GetValue<double>(SupportedConfiguration.CookieExpiredInMinutes);

            // create cookie
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, username, DateTime.Now, DateTime.Now.AddMinutes(cookieExpiredIn), false, data);
            string encryptedTicket = FormsAuthentication.Encrypt(ticket);
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

            // attach cookie in response
            Response.Cookies.Add(cookie);
        }

        [AllowAnonymous]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Template");
        }

        [AllowAnonymous]
        public ActionResult GetLoginUrl(string returnUrl)
        {
            string loginUrl = FormsAuthentication.LoginUrl;
            loginUrl += "?returnUrl=" + returnUrl;
            return Json(loginUrl, JsonRequestBehavior.AllowGet);
        }


    }
}