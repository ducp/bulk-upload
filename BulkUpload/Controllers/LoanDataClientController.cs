﻿using BulkUpload.Entities;
using BulkUpload.Helpers;
using BulkUpload.Models;
using BulkUpload.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BulkUpload.Controllers
{
    public class LoanDataClientController : BaseController
    {
        private readonly IClientService _clientService;
        private readonly IClientLoanService _clientLoanService;
        private readonly IBreezeLoanService _breezeLoanService;
        private readonly IRegisteredLoanService _registeredLoanService;
        private readonly IConfigurableBreezePropertyService _breezePropertyService;

        public LoanDataClientController(
            IClientService clientService,
            IClientLoanService clientLoanService,
            IBreezeLoanService breezeLoanService,
            IRegisteredLoanService registeredLoanService,
            IConfigurableBreezePropertyService breezePropertyService)
        {
            _clientService = clientService;
            _clientLoanService = clientLoanService;
            _breezePropertyService = breezePropertyService;
            _registeredLoanService = registeredLoanService;
            _breezeLoanService = breezeLoanService;
        }

        // GET: LoanClient
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> GetAllLoanData(string keyword = "")
        {
            var clients = await _clientService.GetAllClients();
            var loanData = await _clientLoanService.GetClientLoans(clients, keyword);
            return PartialView("_List", loanData);
        }

        public async Task<ActionResult> GetClientLoanNumberById(int clientLoanId, string commitmentNumber)
        {
            ViewBag.ClientLoanId = clientLoanId;
            ViewBag.CommitmentNumber = commitmentNumber;
            ViewBag.ClientLoanNumber = await _clientLoanService.GetClientLoanNumberById(clientLoanId);
            return PartialView("_Delete");
        }

        [HttpDelete]
        public async Task<ActionResult> DeleteLoan(int clientLoanId)
        {
            try
            {
                await _clientLoanService.DeleteLoan(clientLoanId);
                return Json(new
                {
                    isSuccess = true
                });
            }
            catch (Exception e)
            {
                Log.Instance.Error(e);
                return Json(new
                {
                    isSuccess = false,
                    errorMessage = e.Message
                });
            }
        }

        public async Task<JsonResult> RegisterClientLoanInBreeze(string clientLoanIdJsonString)
        {
            var clienLoanIds = JsonConvert.DeserializeObject<List<int>>(clientLoanIdJsonString);

            List<Task<RegisteredLoanModel>> tasks = new List<Task<RegisteredLoanModel>>();
            foreach (var clienLoanId in clienLoanIds)
            {
                tasks.Add(_breezeLoanService.RegisterLoanInBreeze(clienLoanId));
            }

            Task.WaitAll(tasks.ToArray());

            // link Breeze Loan Id with Client Loan in Bulk Upload
            var result = await _clientLoanService.RegisterBreezeLoan(tasks.Select(x => new RegisteredLoanModel
            {
                BreezeLoanId = x.Result.BreezeLoanId,
                ClientLoanId = x.Result.ClientLoanId
            }).ToList());

            // export loans to a file
            var exportedFile = await _clientLoanService.ExportLoansFile(clienLoanIds, _breezePropertyService.GetBreezeProperties());
            Log.Instance.Info($"exported loan file: {exportedFile}");

            return Json(result.Select(x => new
            {
                clientLoanNumber = x.LoanNumber,
                breezeLoanId = x.BreezeLoanId,
                clientLoanId = x.ClientLoanId
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetLoanDetailModalContent(int clientLoanId)
        {
            ClientLoan clientLoan = await _clientLoanService.GetById(clientLoanId);
            var props = GetLoanPropertiesFromClientLoan(clientLoan);
            return PartialView("_Detail", props);
        }

        public async Task<ActionResult> GetRegisteredLoanDetailModalContent(int clientLoanId)
        {
            var loan = await _registeredLoanService.GetByClientLoanId(clientLoanId);
            ViewBag.BreezeLoanNumber = loan.BreezeLoanId;
            var props = GetLoanPropertiesFromClientLoan(loan.ClientLoan);
            return PartialView("_Detail", props);
        }

        private List<LoanProp> GetLoanPropertiesFromClientLoan(ClientLoan clientLoan)
        {
            var breezeProperties = _breezePropertyService.GetBreezeProperties();
            return (from breezeProp in breezeProperties
                    let lp = clientLoan.LoanProperties?.FirstOrDefault(p => p.PropertyName.ToLower().Equals(breezeProp.PropertyName.ToLower()))
                    let breezePropName = lp?.PropertyName ?? string.Empty
                    let mappings = clientLoan.ImportSession?.Template?.ColumnMappings
                    let isCorrespondentId = breezeProp.PropertyName == "CorrespondentId"
                    let correspondentIdMapping = mappings?.FirstOrDefault(x => x.LoanDataPropertyName == "CorrespondentId")
                    let displayOrder = isCorrespondentId 
                                        ? (correspondentIdMapping == null ? -2 : correspondentIdMapping.ClientColumnDisplayOrder)
                                        : mappings?.FirstOrDefault(x => x.LoanDataPropertyName == breezePropName)?.ClientColumnDisplayOrder ?? 1000
                    let stringValue = lp?.Value ?? string.Empty
                    select new LoanProp
                    {
                        BreezePropName = breezeProp.PropertyName,
                        BreezeColumnName = breezeProp.DisplayName,
                        StringValue = stringValue,
                        DisplayOrder = displayOrder,
                        DisplayValue = breezeProp.BreezeStringHandler(stringValue).GetDisplayString()
                    }).OrderBy(x => x.DisplayOrder).ThenBy(x => x.BreezeColumnName).ToList();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetLoanDetailModalContentByExtractedData(string rowDataString)
        {
            var rowData = JsonConvert.DeserializeObject<ClientLoanDataModel>(rowDataString);
            ViewBag.DuplicatedLoanImportedDate = rowData.DuplicatedLoanImportedDate;
            return PartialView("_Detail", rowData.LoanProps.OrderBy(x => x.DisplayOrder).ThenBy(x => x.BreezeColumnName).ToList());
        }

        [HttpPost]
        public async Task<JsonResult> GeneratePreviewLoansFile(List<int> clientLoanIds)
        {
            var props = _breezePropertyService.GetBreezeProperties();
            string filename = await _clientLoanService.GeneratePreviewFile(clientLoanIds, props);
            return Json(filename, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DownloadPreviewLoansFile(string filename)
        {
            string fullPath = _clientLoanService.GetPreviewDataFileLocation(filename);
            var name = System.IO.Path.GetFileName(filename);
            return File(fullPath, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", name);
        }

        [HttpPost]
        public async Task<JsonResult> ExportLoansToCsvFile(List<int> clientLoanIds)
        {
            var props = _breezePropertyService.GetBreezeProperties();
            string filename = await _clientLoanService.ExportLoansFile(clientLoanIds, props);
            return Json(filename, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DownloadExportedCsvDataFile(string filename)
        {
            string fullPath = _clientLoanService.GetExportedDataFileLocation(filename);
            var name = System.IO.Path.GetFileName(filename);
            return File(fullPath, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", name);
        }
    }
}