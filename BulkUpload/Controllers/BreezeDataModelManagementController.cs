﻿using BulkUpload.Helpers;
using BulkUpload.Models;
using BulkUpload.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BulkUpload.Controllers
{
    [SSOAuthorize]
    public class BreezeDataModelManagementController : BaseController
    {
        public readonly IConfigurableBreezePropertyService _breezePropertyService;

        public BreezeDataModelManagementController(IConfigurableBreezePropertyService breezePropertyService)
        {
            _breezePropertyService = breezePropertyService;
        }

        // GET: BreezeDataModelManagement
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListBreezeDataModelManagement()
        {
            var props = _breezePropertyService.GetBreezeProperties();
            if (!props.Any())
            {
                props = _breezePropertyService.InitBreezePropertiesFromLoanDataModel();
            }
            ViewBag.UndeletableProperties = Utils.GetEnumValues<UndeletableProperties>();
            return PartialView("_List", props);
        }

        public ActionResult GetEditView(string propertyName = "")
        {
            var model = new BreezeProperty();
            if (!string.IsNullOrEmpty(propertyName))
            {
                model = _breezePropertyService.GetBreezeProperties()?.FirstOrDefault(x => x.PropertyName.Equals(propertyName)) ?? new BreezeProperty();
            }

            ViewBag.DataTypes = ConfigurationProvider.SupportedDataTypes
                                                     .Select(x => new SelectListItem { Text = x.Key, Value = x.Key }).ToList();

            return PartialView("_Edit", model);
        }

        public ActionResult GetView(string propertyName)
        {
            var model = _breezePropertyService.GetBreezeProperties()?.FirstOrDefault(x => x.PropertyName.Equals(propertyName)) ?? new BreezeProperty();
            ViewBag.DataTypes = ConfigurationProvider.SupportedDataTypes.Select(x => new SelectListItem { Text = x.Key, Value = x.Key }).ToList();
            ViewBag.UndeletableProperties = Utils.GetEnumValues<UndeletableProperties>();
            return PartialView("_View", model);
        }

        [HttpPost]
        public JsonResult Add(BreezeProperty breezeProperty)
        {
            var result = _breezePropertyService.Add(breezeProperty);
            return Json(new
            {
                isSuccess = result.IsSuccess,
                errorMessage = result.ErrorMessage
            });
        }

        [HttpPost]
        public JsonResult Update(BreezeProperty breezeProperty)
        {
            var result = _breezePropertyService.Update(breezeProperty);
            return Json(new
            {
                isSuccess = result.IsSuccess,
                errorMessage = result.ErrorMessage
            });
        }

        [HttpDelete]
        public JsonResult Delete(string propertyName)
        {
            var result = _breezePropertyService.Delete(propertyName);
            return Json(new
            {
                isSuccess = result.IsSuccess,
                errorMessage = result.ErrorMessage
            });
        }

        public ActionResult GetEditOptionView()
        {
            return PartialView("_Option", string.Empty);
        }
    }
}