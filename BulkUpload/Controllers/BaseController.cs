﻿using BulkUpload.Helpers;
using Newtonsoft.Json;
using System.Web.Mvc;
using System.Web.Security;

namespace BulkUpload.Controllers
{
    //[SSOAuthorize]
    public class BaseController : Controller
    {
        protected override void OnException(ExceptionContext filterContext)
        {
            Log.Instance.Error($"Exception with user: {Username}", filterContext.Exception);
            base.OnException(filterContext);
        }

        protected string Username => GetCurrentUsername();

        private string GetCurrentUsername()
        {
            try
            {
                var cookie = HttpContext.Request.Cookies[0];
                if (!string.IsNullOrEmpty(cookie?.Value ?? string.Empty))
                {
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);
                    var data = JsonConvert.DeserializeObject<BulkUpload.Models.BreezeUserData>(ticket.UserData);
                    return data.Username;
                }

                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }
    }

    public class SSOAuthorizeAttribute : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            //string loginUrl = FormsAuthentication.LoginUrl;
            //loginUrl += "?returnUrl=" + filterContext.HttpContext.Request.Url.AbsoluteUri;
            //if (filterContext.HttpContext.Request.IsAjaxRequest())
            //{
            //    filterContext.HttpContext.SkipAuthorization = true;
            //    filterContext.HttpContext.Response.Clear();
            //    filterContext.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
            //    filterContext.Result = new HttpUnauthorizedResult("AuthorizationMessage");
            //    filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
            //    filterContext.HttpContext.Response.End();
            //}
            //else
            //{
            //    filterContext.Result = new RedirectResult(loginUrl);
            //}
        }
    }
}