﻿using BulkUpload.Helpers;
using BulkUpload.Models;
using BulkUpload.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BulkUpload.Controllers
{
    public class RegisteredLoanController : BaseController
    {
        private readonly IClientService _clientService;
        private readonly IRegisteredLoanService _registeredLoanService;
        private readonly IConfigurableBreezePropertyService _breezePropertyService;

        public RegisteredLoanController(IRegisteredLoanService registeredLoanService, IClientService clientService, IConfigurableBreezePropertyService breezePropertyService)
        {
            _clientService = clientService;
            _registeredLoanService = registeredLoanService;
            _breezePropertyService = breezePropertyService;
        }
        // GET: RegisteredLoan
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> GetAllRegisteredLoan()
        {
            var clientIds = (await _clientService.GetAllClients()).Select(x => x.ClientId).ToList();
            var registeredLoans = await _registeredLoanService.GetAllRegisteredLoan(clientIds);
            var clients = await _clientService.GetAllClients();
            var loans = registeredLoans.Select(x => new RegisteredLoanModel
            {
                ClientLoanId = x.ClientLoanId,
                BreezeLoanId = x.BreezeLoanId,
                RegisteredDate = x.RegisteredDate,
                LoanNumber = x.ClientLoan.LoanProperties.ToList().GetClientLoanNumberFromProperties(),
                Client = clients.FirstOrDefault(c => c.ClientId == x.ClientLoan.ImportSession.ClientId)?.Name
            }).OrderBy(x => x.Client)
            .ThenByDescending(x => x.RegisteredDate)
            .ThenBy(x => x.LoanNumber)
            .ToList();

            return PartialView("_List", loans);
        }

        //[HttpPost]
        //public async Task<JsonResult> ExportLoansToCsvFile(List<int> clientLoanIds)
        //{
        //    string filename = await _registeredLoanService.ExportLoansToCsvFile(clientLoanIds, _breezePropertyService.GetBreezeProperties());
        //    return Json(filename, JsonRequestBehavior.AllowGet);
        //}

        //public ActionResult DownloadExportedCsvDataFile(string filename)
        //{
        //    string fullPath = _registeredLoanService.GetExportedDataFileLocation(filename);
        //    return File(fullPath, "text/csv", filename);
        //}
    }
}