﻿
using BulkUpload.Entities;
using BulkUpload.Models;
using BulkUpload.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BulkUpload.Controllers
{
    public class TemplateController : BaseController
    {
        private readonly ITemplateService _templateService;
        private readonly IClientService _clientService;
        private readonly IColumnMappingService _columnMappingService;

        public TemplateController(ITemplateService templateService, IClientService clientService, IColumnMappingService columnMappingService)
        {
            _templateService = templateService;
            _clientService = clientService;
            _columnMappingService = columnMappingService;
        }

        // GET: Template
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> GetTemplateList()
        {
            var templates = await _templateService.GetAllTemplates();
            var clients = await _clientService.GetAllClients();
            return PartialView("_List", templates.Select(x => new TemplateModel
            {
                ClientId = x.ClientId,
                ClientName = clients.FirstOrDefault(c => c.ClientId == x.ClientId)?.Name ?? string.Empty,
                ColumnMappings = x.ColumnMappings.ToList(),
                TemplateId = x.TemplateId,
                TemplateName = x.TemplateName,
                UploadFile = x.UploadFile
            }).ToList());
        }

        public ActionResult Detail(int id = 0)
        {
            ViewBag.TemplateId = id;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> Update(int templateId, string templateName, int clientId, string columnMappings)
        {
            var result = await _templateService.UpdateTemplate(templateId, templateName, clientId, columnMappings, Request.Files);
            return Json(new
            {
                isSuccess = result.IsSuccess,
                errorMessage = result.ErrorMessage
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> Create(string templateName, int clientId, string columnMappings)
        {
            var result = await _templateService.CreateTemplate(templateName, clientId, columnMappings, Request.Files);
            return Json(new
            {
                isSuccess = result.IsSuccess,
                errorMessage = result.ErrorMessage
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> CheckTemplateName(string newTemplateName, int fromClientid, int templateId = 0)
        {
            var template = await _templateService.GetTemplateByName(fromClientid, newTemplateName);
            return Json(template == null || template.TemplateId == templateId, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetTemplateById(int templateId)
        {
            var data = await _templateService.GetTemplateInfoById(templateId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpDelete]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> DeleteTemplate(int templateId)
        {
            await _templateService.Delete(templateId);
            return Json(new { isSuccess = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> CloneTemplate(int fromTemplateId, string newTemplateName, int fromClientid)
        {
            if (await _templateService.GetTemplateByName(fromClientid, newTemplateName) != null)
            {
                return Json(new { isSuccess = false, errorMessage = $"The name {newTemplateName} is already in use" });
            }

            await _templateService.CloneTemplate(fromTemplateId, newTemplateName, fromClientid);

            return Json(new { isSuccess = true, message = $"Clone {newTemplateName} success" });
        }

        public async Task<JsonResult> GetClientOptionList()
        {
            var clients = await _clientService.GetAllClients();
            return Json(clients.Select(x => new { text = x.Name, value = x.ClientId.ToString() }), JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetTemplatesByClientId(int clientId)
        {
            var templates = await _templateService.GetByClientId(clientId);
            return Json(templates.Select(x => new { text = x.TemplateName, value = x.TemplateId }), JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetColumnMappingsByTemplate(int templateId)
        {
            var col = await _columnMappingService.GetColumnMappingsByTemplate(templateId);
            return PartialView("_Mapping", col);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ClientFileUpload()
        {
            HttpFileCollectionBase files = Request.Files;
            HttpPostedFileBase file = files[0];
            var headers = _columnMappingService.GetHeaderColumns(file);
            return Json(headers, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOptionBreezeColumnName()
        {
            var props = _columnMappingService.GetOptionsBreezeColumns();
            return Json(props, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RenderColumnMappingsView(string clientColumns, string breezeColumns, string mappings)
        {
            var clientCells = JsonConvert.DeserializeObject<List<CellModel>>(clientColumns);
            var clientColOptions = clientCells.Select(x => new SelectListItem { Value = x.Value, Text = x.Name }).ToList();
            clientColOptions.Insert(0, new SelectListItem { Value = string.Empty, Text = string.Empty });
            ViewBag.ClientColumns = clientColOptions;

            var breezeCols = JsonConvert.DeserializeObject<List<SelectListItemModel>>(breezeColumns);
            var breezeColumnOptions = breezeCols.Select(x => new SelectListItem { Value = x.Value, Text = x.Text }).ToList();
            breezeColumnOptions.Insert(0, new SelectListItem { Value = string.Empty, Text = string.Empty });
            ViewBag.BreezeColumns = breezeColumnOptions;

            var templateMappings = JsonConvert.DeserializeObject<List<ColumnMapping>>(mappings);
            return PartialView("_TemplateMappings", templateMappings.Select(x => new TemplateMapping
            {
                ClientColumnName = x.ClientColumnName,
                BreezeColumnName = x.BreezeColumnName,
                LoanDataPropertyName = x.LoanDataPropertyName,
                IsMandatory = breezeCols.FirstOrDefault(b => b.Value == x.LoanDataPropertyName)?.Mandatory ?? false
            }).ToList());
        }

        [HttpPost]
        public ActionResult RenderNewColumnMappingView(string clientColumns, string breezeColumns)
        {
            var clientCells = JsonConvert.DeserializeObject<List<CellModel>>(clientColumns);
            var clientColOptions = clientCells.Select(x => new SelectListItem { Value = x.Value, Text = x.Name }).ToList();
            clientColOptions.Insert(0, new SelectListItem { Value = string.Empty, Text = string.Empty });
            ViewBag.ClientColumns = clientColOptions;

            var breezeCols = JsonConvert.DeserializeObject<List<SelectListItemModel>>(breezeColumns);
            var breezeColumnOptions = breezeCols.Select(x => new SelectListItem { Value = x.Value, Text = x.Text }).ToList();
            breezeColumnOptions.Insert(0, new SelectListItem { Value = string.Empty, Text = string.Empty });
            ViewBag.BreezeColumns = breezeColumnOptions;
            return PartialView("_TemplateMapping", new TemplateMapping());
        }
    }
}