﻿function BreezePropertyService() {
    var apiCaller = new ApiCaller('/BreezeDataModelManagement');
    return {
        getEditView: function (propertyName) {
            return apiCaller.get('GetEditView', { propertyName: propertyName });
        },
        getListBreezeDataModelManagement: function () {
            return apiCaller.get('GetListBreezeDataModelManagement', null);
        },
        deleteBreezeProperty: function (propertyName) {
            return apiCaller.delete('Delete', { propertyName: propertyName });
        },
        addBreezeProperty: function (property) {
            return apiCaller.post('Add', property);
        },
        updateBreezeProperty: function (property) {
            return apiCaller.post('Update', property);
        },
        getView: function (propertyName) {
            return apiCaller.get('GetView', { propertyName: propertyName });
        },
        getEditOptionView: function () {
            return apiCaller.get('GetEditOptionView', {});
        }
    };
}