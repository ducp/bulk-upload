﻿
function RequestObj(t, e, d, isFormData) {
    var method = t;
    var url = e;
    var data = d;
    var usingFormData = isFormData;
    return {
        callApi: function (successCallback, errorCallback, beforeSend, complete) {
            var obj = {
                method: method,
                cache: false,
                url: url,
                data: data,
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status === 401) { return; }
                    successCallback(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status === 401) { return; }
                    errorCallback(errorThrown);
                },
                statusCode: {
                    401: function () {
                        $.ajax({
                            method: 'GET',
                            url: '/SSOAuthenticate/GetLoginUrl',
                            data: { returnUrl: encodeURI(window.location.href) },
                            success: function (loginUrl) {
                                window.location.href = loginUrl;
                            },
                            error: function (err) {
                                console.log(err);
                            }
                        });
                    }
                }
            };

            if (usingFormData) {
                obj.contentType = false;
                obj.processData = false;
            }

            if (beforeSend) {
                obj.beforeSend = beforeSend;
            }

            if (complete) {
                obj.complete = complete;
            }

            $.ajax(obj);
        }
    };
}

function ApiCaller(controller) {

    var self = this;

    var baseUrl = controller;

    function createUrl(api) {
        let applicationName = $('[name=ApplicationName]').val();
        return `${applicationName ? '/' : ''}${applicationName}${baseUrl}${baseUrl.endsWith('/') ? '' : '/'}${api}`;
    }

    self.get = function (api, data, beforeSend, complete) {
        var url = createUrl(api);
        var df = jQuery.Deferred();
        var requestObj = new RequestObj('GET', url, data);
        requestObj.callApi(
            function (response) { df.resolve(response); },
            function (error) { df.reject(error); },
            beforeSend,
            complete);
        return df.promise();
    };

    self.post = function (api, data, beforeSend, complete) {
        var url = createUrl(api);
        data = data || {};
        data["__RequestVerificationToken"] = $('input[name="__RequestVerificationToken"]').val();
        var df = jQuery.Deferred();
        var requestObj = new RequestObj('POST', url, data);
        requestObj.callApi(
            function (response) { df.resolve(response); },
            function (error) { df.reject(error); },
            beforeSend,
            complete);
        return df.promise();
    };

    self.put = function (api, data, beforeSend, complete) {
        var url = createUrl(api);
        var df = jQuery.Deferred();
        var requestObj = new RequestObj('PUT', url, data);
        requestObj.callApi(
            function (response) { df.resolve(response); },
            function (error) { df.reject(error); },
            beforeSend,
            complete);
        return df.promise();
    };

    self.delete = function (api, data, beforeSend, complete) {
        var url = createUrl(api);
        data = data || {};
        data["__RequestVerificationToken"] = $('input[name="__RequestVerificationToken"]').val();
        var df = jQuery.Deferred();
        var requestObj = new RequestObj('DELETE', url, data);
        requestObj.callApi(
            function (response) { df.resolve(response); },
            function (error) { df.reject(error); },
            beforeSend,
            complete);
        return df.promise();
    };

    self.postFormData = function (api, data, beforeSend, complete) {
        var url = createUrl(api);
        data = data || new FormData();
        data.append("__RequestVerificationToken", $('input[name="__RequestVerificationToken"]').val());
        var df = jQuery.Deferred();
        var requestObj = new RequestObj('POST', url, data, true);
        requestObj.callApi(
            function (response) { df.resolve(response); },
            function (error) { df.reject(error); },
            beforeSend,
            complete);
        return df.promise();
    };

    return self;
}

function Notifier() {
    function notify(message, type) {
        $.notify({
            // options
            icon: 'glyphicon glyphicon-bell',
            message: message
        }, {
            type: type
        });
    }
    return {
        showSuccess: function (message) {
            notify(message, 'success');
        },
        showWarning: function (message) {
            notify(message, 'warning');
        },
        showError: function (message) {
            notify(message, 'danger');
        }
    };
}

if (!window.notifier) {
    window.notifier = new Notifier();
}

function ModalConfirm(targetModalId, onloaded, callback) {

    this.targetModalId = targetModalId;
    this.onloaded = onloaded;
    this.callback = callback;

    this.open = function () {
        $('#' + this.targetModalId).modal('show');
        if (this.onloaded) {
            this.onloaded();
        }
    };

    this.onConfirmed = function () {
        $('#' + this.targetModalId).modal('hide');
        this.callback(true);
    };

    this.onDeclined = function () {
        $('#' + this.targetModalId).modal('hide');
        this.callback(false);
    };
}

var openConfirmModal = function (targetModalId, onloaded, callback) {
    window.confirmModalInstance = new ModalConfirm(targetModalId, onloaded, callback);
    window.confirmModalInstance.open();
    $('[name=confirm-modal-confirmed]').on('click', function () {
        if (window.confirmModalInstance) {
            window.confirmModalInstance.onConfirmed();
            window.confirmModalInstance = undefined;
        }
    });
    $('[name=confirm-modal-declined]').on('click', function () {
        if (window.confirmModalInstance) {
            window.confirmModalInstance.onDeclined();
            window.confirmModalInstance = undefined;
        }
    });
}