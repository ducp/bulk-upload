﻿
function TemplateService() {
    var apiCaller = new ApiCaller('/Template');
    return {
        getTemplatesByClient: function (clientId) {
            return apiCaller.get('GetTemplatesByClientId', { clientId: clientId });
        },

        cloneTemplate: function (data, beforeSend) {
            return apiCaller.post('CloneTemplate', data, beforeSend);
        },

        deleteTemplate: function (data, beforeSend) {
            return apiCaller.delete('DeleteTemplate', data, beforeSend);
        },

        getTemplateById: function (templateId) {
            return apiCaller.get('GetTemplateById', { templateId: templateId });
        },

        getBreezeColumnName: function () {
            return apiCaller.get('GetOptionBreezeColumnName', null);
        },

        getColumnMappingsByTemplate: function (templateId, beforeSend) {
            return apiCaller.get('GetColumnMappingsByTemplate', { templateId: templateId }, beforeSend);
        },

        getTemplateList: function () {
            return apiCaller.get('GetTemplateList', null);
        },

        getClientHeaderFromFile: function (data, beforeSend) {
            return apiCaller.postFormData('ClientFileUpload', data, beforeSend);
        },

        create: function (data, beforeSend) {
            return apiCaller.postFormData('Create', data, beforeSend);
        },

        update: function (data, beforeSend) {
            return apiCaller.postFormData('Update', data, beforeSend);
        },

        checkExistTemplateName: function (data, beforeSend) {
            return apiCaller.post('CheckTemplateName', data, beforeSend);
        },

        getColumnMappingsView: function (formData) {
            return apiCaller.postFormData("RenderColumnMappingsView", formData);
        },

        getNewColumnMappingView: function (formData) {
            return apiCaller.postFormData("RenderNewColumnMappingView", formData);
        }
    };
}


