﻿
function LoanDataService() {
    var apiCaller = new ApiCaller('/LoanDataClient');
    return {
        getClientLoans: function (keyword, beforeSend, complete) {
            return apiCaller.get('GetAllLoanData', { keyword: keyword }, beforeSend, complete);
        },
        getLoanDetails: function (clientLoanId, beforeSend) {
            return apiCaller.get('GetLoanDetail', { clientLoanId: clientLoanId }, beforeSend);
        },
        getDeleteLoanConfirmationModalContent: function (clientLoanId, commitmentNumber, beforeSend) {
            return apiCaller.get('GetClientLoanNumberById', { clientLoanId: clientLoanId, commitmentNumber: commitmentNumber }, beforeSend);
        },
        deleteLoan: function (clientLoanId) {
            return apiCaller.delete('DeleteLoan?clientLoanId=' + clientLoanId, null);
        },
        registerClientLoanInBreeze: function (clientLoanIds) {
            return apiCaller.post('RegisterClientLoanInBreeze', { clientLoanIdJsonString: JSON.stringify(clientLoanIds) });
        },
        generatePreviewLoansFile: function (clientLoanIds) {
            return apiCaller.post('GeneratePreviewLoansFile', { clientLoanIds: clientLoanIds });
        },
        downloadPreviewLoansFile: function (filename) {
            return apiCaller.get('DownloadPreviewLoansFile?filename=' + filename);
        }
    };
}

