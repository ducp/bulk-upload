﻿
function LoanService() {
    var apiCaller = new ApiCaller('/LoanDataClient');
    return {

        getRegisteredLoanDetailModalContent: function (clientLoanId) {
            return apiCaller.get('GetRegisteredLoanDetailModalContent', { clientLoanId: clientLoanId });
        },

        getLoanDetailModalContent: function (clientLoanId) {
            return apiCaller.get('GetLoanDetailModalContent', { clientLoanId: clientLoanId });
        },

        getLoanDetailModalContentByExtractedData: function (rowDataString) {
            return apiCaller.post('GetLoanDetailModalContentByExtractedData', { rowDataString: rowDataString });
        }
    };
}
