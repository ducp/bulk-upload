﻿function RegisteredLoanService() {
    var apiCaller = new ApiCaller('/RegisteredLoan');
    return {

        getAllRegisteredLoan : function () {
            return apiCaller.get('GetAllRegisteredLoan', null);
        },

        exportLoansToFile : function (clientLoanIds) {
            return apiCaller.post('ExportLoansToCsvFile', { clientLoanIds: clientLoanIds });
        },

        downloadExportedFile : function (filename) {
            window.location.href = '/RegisteredLoan/DownloadExportedCsvDataFile?filename=' + filename;
        }
    };
   

}