﻿
function CommitLoanService() {
    var apiCaller = new ApiCaller('/ImportLoan');
    return {

        loadPreviewContent: function (formData, beforeSend) {
            return apiCaller.postFormData('LoadPreviewContent', formData, beforeSend);
        },

        importLoans: function (formData) {
            return apiCaller.postFormData('ImportLoans', formData);
        },

        generateErrorLoansFile: function (formData) {
            return apiCaller.postFormData('GenerateErrorLoansFile', formData);
        }

    };
}
