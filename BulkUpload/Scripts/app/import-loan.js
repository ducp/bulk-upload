﻿
function CommitLoanPage() {

    var clientService = new ClientService();
    var templateService = new TemplateService();
    var loanService = new LoanService();
    var commitLoanService = new CommitLoanService();
    var selectedImportFile = null;

    var loans = [];
    var selectedToImport = [];
    var importSessionId = 0;

    function initPage() {
        $('#loading').append($('<i>').addClass('fa fa-spinner fa-spin').css('font-size', '60px'));
        $('#divUserSelections').addClass('hide');
        clientService.getAllClients()
            .then(function (options) {
                $.each(options, function (index, option) {
                    $('#optClient').append('<option class="dropdown-item" value="' + option.value + '">' + option.text + '</option>');
                });
                $('#loading').html('');
                $('#divUserSelections').addClass('show');
            })
            .catch(function (error) {
                $('#loading').html('');
            });
    }

    function beforeSendPreview() {
        $('#btnPreview').attr('disabled', 'disabled');
        $('#divPreviewClientLoans').html('');
        $('#divPreviewClientLoans').append($('<i>').addClass('fa fa-spinner fa-spin').css('font-size', '60px'));
        $('#generatePreviewContentFailure').hide();
    }

    function loadScroolbar() {
        $(".wrapper1").scroll(function () {
            $(".wrapper2")
                .scrollLeft($(".wrapper1").scrollLeft());
        });
        $(".wrapper2").scroll(function () {
            $(".wrapper1")
                .scrollLeft($(".wrapper2").scrollLeft());
        });
    }

    function loadPreviewContent() {
        var formData = new FormData();
        formData.append(selectedImportFile.name, selectedImportFile);
        formData.append('clientId', $('#optClient').val());
        formData.append('templateId', $('#optTemplate').val());

        commitLoanService.loadPreviewContent(formData, beforeSendPreview)
            .then(function (response) {

                $('#divPreviewClientLoans').html(response);
                $('.error-cell').tooltip();
                loans = JSON.parse($('#hiddenClientLoansJsonString').val());
            })
            .catch(function (error) {
                $('#generatePreviewContentFailure').show();
            })
            .done(function () {
                $('#btnPreview').removeAttr('disabled');
                $('#divPreviewClientLoans').find('i.fa-spinner').remove();
                $('[name=preview-header]').tooltip();
                loadScroolbar();
            });
    }

    function handleImportResultUI(successfullyLoans) {
        loans = loans.filter(item => successfullyLoans.indexOf(item.lineNumber) === -1);
        selectedToImport = [];
        handleDisabledAttrForImportLoanButton();
        for (var i = 0; i < successfullyLoans.length; i++) {
            $('tr[name=clientLoanRow_' + successfullyLoans[i] + ']').remove();
        }
    }

    function importLoans(loans) {

        var formData = new FormData();
        formData.append(selectedImportFile.name, selectedImportFile);
        formData.append('clientId', $('#optClient').val());
        formData.append('templateId', $('#optTemplate').val());
        formData.append('importSessionId', importSessionId);
        formData.append('loansString', JSON.stringify(loans));

        $('#modalWithLoadingSpinner').modal();
        commitLoanService.importLoans(formData)
            .then(function (response) {
                $('#modalWithLoadingSpinner').find('.modal-content').first().html(response);
                importSessionId = $('#modalWithLoadingSpinner').find('[name=importSessionId]').val();
                handleImportResultUI(JSON.parse($('#modalWithLoadingSpinner').find('[name=successfullyLoans]').val()));
            })
            .catch(function (error) {
                $('#modalWithLoadingSpinner').modal('hide');
                notifier.showError('Failed to commit loans.');
            });
    }

    function reloadPreview() {
        $('#divPreviewClientLoans').html('');
        if (canLoadPreview()) {
            loadPreviewContent();
        }
    }

    function canLoadPreview() {
        var clientId = $('#optClient').val();
        var templateId = $('#optTemplate').val();
        return clientId > 0 && templateId > 0 && selectedImportFile;
    }

    function handleDisabledAttrForImportLoanButton() {
        if (selectedToImport.length > 0) {
            $('#btnImportLoan').removeAttr('disabled');
        }
        else {
            $('#btnImportLoan').attr('disabled', 'disabled');
        }
    }

    function onSelectToImportChange(elm) {
        if ($(elm).prop('checked')) {
            selectedToImport.push($(elm).data('line-number'));
        }
        else {
            selectedToImport = selectedToImport.filter(item => item !== $(elm).data('line-number'));
        }
        handleDisabledAttrForImportLoanButton();
    }

    function onSelectAllToImportChange(elm) {
        if ($(elm).prop('checked')) {
            selectedToImport = loans.filter(item => !item.invalidDataRow).map(item => item.lineNumber);
            $('[id^=chkSelectToImport_]').each(function () { $(this).prop('checked', true); });
        }
        else {
            selectedToImport = [];
            $('[id^=chkSelectToImport_]').each(function () { $(this).prop('checked', false); });
        }
        handleDisabledAttrForImportLoanButton();
    }

    function onClientChange(elm) {
        $('#optTemplate').find('option[value!=0]').remove();
        $('#optTemplate').val(0);
        var clientId = $(elm).val();
        if (clientId !== '0') {
            templateService.getTemplatesByClient(clientId)
                .then(function (options) {
                    $.each(options, function (index, option) {
                        $('#optTemplate').append('<option class="dropdown-item" value="' + option.value + '">' + option.text + '</option>');
                    });
                })
                .catch(function (error) {
                    notifier.showError('Error while loading client list.');
                });
        }
        reloadPreview();
    }

    function onTemplateChange(elm) {
        $('#divPreviewClientLoans').html('');
        reloadPreview();
    }

    function onFileChange(elm) {
        var uploader = elm;
        if (uploader.files.length > 0) {
            $('#file-return').val(uploader.files[0].name);
            selectedImportFile = uploader.files[0];
            reloadPreview();
        }
    }

    function commitLoans() {

        handleDisabledAttrForImportLoanButton();

        var selectedLoans = loans.filter(function (item) { return selectedToImport.indexOf(item.lineNumber) !== -1; });
        if (selectedLoans.length > 0) {
            let duplicatedLoans = selectedLoans.filter(function (loan) { return loan.isDuplicated === true; });
            if (duplicatedLoans.length === 0) {
                importLoans(selectedLoans);
            } else {
                var buildDuplicatedLoansModalContent = function (loans) {
                    let duplicatedLoanList = $('#duplicatedLoanList').html('');
                    $.each(loans, function (index, loan) {
                        duplicatedLoanList.append($('<p>').addClass('').text('- Loan number ' + loan.duplicatedClientLoanNumber + ' was committed on ' + loan.duplicatedLoanImportedDate));
                    });
                };
                openConfirmModal(
                    'overrideLoansConfirmationModal',
                    buildDuplicatedLoansModalContent(duplicatedLoans),
                    function callback(confirmed) {
                        if (confirmed) {
                            importLoans(selectedLoans);
                        }
                    }
                );
            }
        }
    }

    function loadLoanDetails(elm) {
        var lineNumber = $(elm).data('line-number');
        let loan = loans.find(item => item.lineNumber === lineNumber);
        $('#modalWithLoadingSpinner').modal();
        loanService.getLoanDetailModalContentByExtractedData(JSON.stringify(loan))
            .then(function (response) {
                $('#modalWithLoadingSpinner').find('.modal-content').first().html(response);
            })
            .catch(function (error) {
                $('#modalWithLoadingSpinner').modal('hide');
                notifier.showError('Failed to load loan detail.');
            });
    }

    function generateErrorLoansFile() {
        let errorLoans = loans.filter(item => item.invalidDataRow);
        var formData = new FormData();
        formData.append(selectedImportFile.name, selectedImportFile);
        formData.append('loansString', JSON.stringify(errorLoans));
        commitLoanService.generateErrorLoansFile(formData)
            .then(file => {
                debugger;
                window.location.href = `/${$('#ApplicationName').val()}/ImportLoan/DownloadErrorLoansFile?filename=${file}`;
            });
    }

    return {
        init: initPage,
        onClientChange: onClientChange,
        onTemplateChange: onTemplateChange,
        onFileChange: onFileChange,
        onSelectToImportChange: onSelectToImportChange,
        onSelectAllToImportChange: onSelectAllToImportChange,
        loadLoanDetails: loadLoanDetails,
        commitLoans: commitLoans,
        generateErrorLoansFile: generateErrorLoansFile
    };

}

var ThePage = CommitLoanPage();
ThePage.init();
