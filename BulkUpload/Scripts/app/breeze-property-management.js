﻿
var breezeFieldManagementPage = function () {

    this.fieldToDelete = null;
    this.fieldToEdit = null;

    var breezePropertyService = new BreezePropertyService();

    function reloadBreezeDataModelManagementList() {
        $('#lstBreezeColummapping').html('');
        $('#loading').append($('<i>').addClass('fa fa-spinner fa-spin').css('font-size', '60px'));
        breezePropertyService.getListBreezeDataModelManagement()
            .then(function (response) {
                $('#lstBreezeColummapping').html(response);
            })
            .catch(function (error) { notifier.showError(error); })
            .done(function () { $('#loading').html(''); });
    }

    function addField() {
        var $tr_edit = $('tr[name="editProperty"]');
        if ($tr_edit.length > 0) {
            $tr_edit.find('input').first().focus();
            return;
        }
        //. allow adding 1 line at time
        breezePropertyService.getEditView()
            .then(function (response) {
                $(response).insertAfter($('#tableBreezeProperty').find('tr[name=header]').first());
                $('[name="addEnumerationsOption"]').hide();
            });
    }

    function editField(elm) {
        // allow edit 1 line at time
        $tr_edit = $('tr[name="editProperty"]');
        if ($tr_edit.length > 0) {
            $tr_edit.find('input').first().focus();
            return;
        }

        this.fieldToEdit = $(elm).data('property-name');
        let field = this.fieldToEdit;
        breezePropertyService.getEditView(field).then((response) => {
            $('tr[data-property-name="' + field + '"]').first().replaceWith($(response));
            changeDataType($(response).find('[name=DataType]').val());
        });
    }

    function changeDataType(type) {
        var fieldInfoRow = $('tr[name=editProperty]');
        switch (type) {
            case 'STRING':
                fieldInfoRow.find('[name="MaxStringLength"]').show();
                fieldInfoRow.find('[name="RegrexPattern"]').show();
                fieldInfoRow.find('[name="addEnumerationsOption"]').hide();
                fieldInfoRow.find('[name="enumerationsOption"]').hide();
                fieldInfoRow.find('[name="deleteEnumerationsOption"]').hide();
                break;
            case 'ENUMERATIONS':
                fieldInfoRow.find('[name="MaxStringLength"]').hide();
                fieldInfoRow.find('[name="RegrexPattern"]').hide();
                fieldInfoRow.find('[name="addEnumerationsOption"]').show();
                fieldInfoRow.find('[name="enumerationsOption"]').show();
                fieldInfoRow.find('[name="deleteEnumerationsOption"]').show();
                break;
            default:
                fieldInfoRow.find('[name="MaxStringLength"]').hide();
                fieldInfoRow.find('[name="RegrexPattern"]').hide();
                fieldInfoRow.find('[name="addEnumerationsOption"]').hide();
                fieldInfoRow.find('[name="enumerationsOption"]').hide();
                fieldInfoRow.find('[name="deleteEnumerationsOption"]').hide();
                break;
        }
    }

    function cancelEdit(elm) {
        let field = this.fieldToEdit;
        if (!field) {
            $(elm).closest('tr').remove();
        }
        else {
            breezePropertyService.getView(field).then((res) => { $(elm).closest('tr').first().replaceWith($(res)); });
        }
    }

    function saveField(elm) {
        var error = false;
        var edittingRow = $(elm).closest('tr');
        var propNameElm = edittingRow.find('[name=PropertyName]');
        var dislayNameElm = edittingRow.find('[name=DisplayName]');
        var dataType = edittingRow.find('[name=DataType]').val();

        // remove validator
        propNameElm.removeClass("border-error-no-span");
        dislayNameElm.removeClass("border-error-no-span");
        var propertyName = $(elm).data('property-name');
        var action = "Update";
        if (!propertyName) {
            action = "Add";
            propertyName = propNameElm.val();
        }
        if (!propertyName) {
            error = true;
            propNameElm.addClass("border-error-no-span");
        }
        var displayName = dislayNameElm.val();
        if (!displayName) {
            error = true;
            dislayNameElm.addClass("border-error-no-span");
        }
        var optionEnums = edittingRow.find('[name=enumerationsOption]');
        if (dataType === "ENUMERATIONS" && optionEnums.length > 0) {
            $.each(optionEnums, function (idx, opt) {
                $(opt).removeClass("border-error-no-span");
                if (opt.value === "") {
                    $(opt).addClass("border-error-no-span");
                    error = true;
                }
            });
        }
        if (!error) {

            var maxStringLength = dataType === 'STRING' ? edittingRow.find('[name=MaxStringLength]').val() : 0;
            var mandatory = edittingRow.find('[name=Mandatory]').prop('checked');
            var order = edittingRow.find('[name=Order]').val();
            var options = dataType === 'ENUMERATIONS' ? $.map($('[name=enumerationsOption]'), function (item) { return item.value; }) : [];
            var exportUsingName = edittingRow.find('[name=ExportUsingName]').val();
            var regrexPattern = edittingRow.find('[name=RegrexPattern]').val();
            var isCurrency = edittingRow.find('[name=IsCurrency]').prop('checked');
            var roundNumberDown = edittingRow.find('[name=RoundNumberDown]').prop('checked');

            var property = {
                propertyName,
                displayName,
                dataType,
                maxStringLength,
                mandatory,
                order,
                options,
                exportUsingName,
                regrexPattern,
                isCurrency,
                roundNumberDown
            };
            if (action === "Add") {
                addProPerty(property);
            } else {
                editProperty(property);
            }
        }

        function addProPerty(property) {
            breezePropertyService.addBreezeProperty(property)
                .then(function (response) {
                    if (response.isSuccess) {
                        $(edittingRow).remove();
                        notifier.showSuccess("Saved");
                        reloadBreezeDataModelManagementList();
                    } else {
                        notifier.showError(response.errorMessage);
                    }
                })
                .catch(function () { notifier.showError("Failed to create field."); })
                .done(function () {
                    edittingRow.remove();
                });
        }

        function editProperty(property) {
            breezePropertyService.updateBreezeProperty(property)
                .then(function (response) {
                    if (response.isSuccess) {
                        edittingRow.remove();
                        notifier.showSuccess("Saved");
                        reloadBreezeDataModelManagementList();
                        this.fieldToEdit = null;
                    } else {
                        notifier.showError(response.errorMessage);
                    }
                })
                .catch(function () { notifier.showError("Failed to edit field."); })
                .done(function () {
                    edittingRow.remove();
                });
        }
    }

    function deleteField(propertyName) {
        this.fieldToDelete = propertyName;
        $("#lblpropertyName").text(propertyName);
        $("#modalDeleteBreezeProperty").modal("show");
    }

    function deleteFieldConfirmed() {
        let field = this.fieldToDelete;
        breezePropertyService.deleteBreezeProperty(field)
            .then(function (response) {
                if (response.isSuccess) {
                    $("#modalDeleteBreezeProperty").modal("hide");
                    notifier.showSuccess("Deleted field successfully.");
                    $('tr[data-property-name="' + field + '"]').remove();

                } else {
                    notifier.showError(response.errorMessage);
                }
            })
            .catch(function () {
                notifier.showError("Error occurs while delete");
            });
    }

    function addEnumerationsOption(elm) {
        let opt = $(elm);
        breezePropertyService.getEditOptionView()
            .then(function (response) {
                opt.closest('td').find('div').append($(response));
            })
            .catch(function (error) { notifier.showError("Failed to load edit option view."); });
    }

    function deleteEnumerationsOption(elm) {
        $(elm).closest('p').remove();
    }

    return {
        reloadBreezeDataModelManagementList: reloadBreezeDataModelManagementList,
        addField: addField,
        editField: editField,
        cancelEdit: cancelEdit,
        saveField: saveField,
        deleteField: deleteField,
        deleteFieldConfirmed: deleteFieldConfirmed,
        addEnumerationsOption: addEnumerationsOption,
        deleteEnumerationsOption: deleteEnumerationsOption,
        changeDataType: changeDataType
    };
};

var ThePage = breezeFieldManagementPage();
ThePage.reloadBreezeDataModelManagementList();

