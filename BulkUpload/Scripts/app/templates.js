﻿
var templatesPage = function () {

    var clientService = new ClientService();

    var templateService = new TemplateService();

    function reloadTemplateList() {
        $('#lstTemplate').html('');
        $('#loading').append($('<i>').addClass('fa fa-spinner fa-spin').css('font-size', '60px'));

        templateService.getTemplateList()
            .then(function (response) {
                $('#lstTemplate').html(response);
                $(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                });
                $('#loading').html('');
            })
            .catch(function (error) {
                notifier.showError('Failed to get template list.');
            });
    }

    function domBinding() {
        $(document).on("click", "[name=btnColumnMapping]", function () {
            var templateId = $(this).data('column-template-id');
            $('#modalWithLoadingSpinner').modal();
            templateService.getColumnMappingsByTemplate(templateId)
                .then(function (response) {
                    $('#modalWithLoadingSpinner').find('.modal-content').first().html(response);
                })
                .catch(function (error) {
                    $('#modalWithLoadingSpinner').modal('hide');
                    notifier.showError('Failed to get mapping for template.');
                });
        });

        $(document).on("click", "[name=btnDeleteTemplate]", function () {
            var _this = $(this);
            var templateId = _this.data('template-id');
            var templateName = _this.data('template-name');
            $("#templateToDetele").val(templateId);
            $("#templateName").text(templateName);
            $("#deleteTemplateModal").modal("show");
        });

        $(document).on("click", "#deleteTemplate", function () {
            var templateId = $("#templateToDetele").val();

            var data = {
                templateId: templateId
            };
            templateService.deleteTemplate(data,
                function () {
                    $('#deleteTemplate').attr('disabled', 'disabled');
                })
                .then(function (response) {
                    if (response.isSuccess) {
                        $("#deleteTemplateModal").modal("hide");
                        reloadTemplateList();
                        notifier.showSuccess("Successfully deleted template");
                    } else {
                        notifier.showError(response.errorMessage);
                    }
                })
                .catch(function (error) {
                    notifier.showError("Failed to delete template.");
                })
                .done(function () {
                    $('#deleteTemplate').removeAttr('disabled');
                });
        });

        $(document).on('click', '[name=btnCloneTemplate]', function () {
            // clear modal content
            $('#messageerror').hide();
            $('#ddlClient').empty();

            // bind template content to clone modal
            var _ = $(this);
            var fromTemplateId = _.data('from-template-id');
            var fromTemplateName = _.data('from-template-name').toString();
            var fromClientId = _.data('from-client-id');
            $('#myModalLabel').val(fromTemplateId);
            $('#newTemplateName').val(fromTemplateName.trim() + ' (copy)');
            $('#fromTemplateId').val(fromTemplateId);
            $('#cloneTemplateModal').modal();

            clientService.getAllClients()
                .then(function (options) {
                    $.each(options, function (index, option) {
                        $('#ddlClient').append('<option class="dropdown-item" value="' + option.value + '">' + option.text + '</option>');
                    });
                    $('#ddlClient').val(fromClientId);
                })
                .catch(function (error) {
                    notifier.showError('Failed to get client list.');
                });
        });

        // clone
        $(document).on("click", "#cloneTemplate", function () {
            var newTemplateName = $("#newTemplateName").val();
            var fromTemplateId = $("#fromTemplateId").val();
            var fromClientid = $("#ddlClient").val();
            templateService.cloneTemplate(
                {
                    fromTemplateId: fromTemplateId,
                    newTemplateName: newTemplateName,
                    fromClientid: fromClientid
                },
                function () {
                    // clear error message
                    $('#messageerror').hide();
                    // disable button
                    $('#cloneTemplate').attr('disabled', 'disabled');
                })
                .then(function (response) {
                    if (response.isSuccess) {
                        reloadTemplateList();
                        $("#cloneTemplateModal").modal("hide");
                        notifier.showSuccess('Successfully cloned template.');
                    }
                    else {
                        $('#messageerror').text(response.errorMessage).show();
                    }
                })
                .catch(function (error) {
                    notifier.showError('Failed to clone template.');
                })
                .done(function () {
                    // enable button
                    $('#cloneTemplate').removeAttr('disabled');
                });
        });
    }

    return {
        init: function () {
            reloadTemplateList();
            domBinding();
        }()
    };

}();


