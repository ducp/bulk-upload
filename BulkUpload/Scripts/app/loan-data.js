﻿
var loanDataPage = function () {

    var loanService = new LoanService();
    var loanDataService = new LoanDataService();

    var handleOnChangeCkSelect = function () {
        if ($('[name=ckRegisterInBreeze]').filter(function () { return $(this).prop('checked') === true; }).length > 0) {
            $('#btnRegisterInBreeze').removeAttr('disabled');
            $('#btnPreviewLoan').removeAttr('disabled');
        } else {
            $('#btnRegisterInBreeze').attr('disabled', 'disabled');
            $('#btnPreviewLoan').attr('disabled', 'disabled');
        }
    }; 

    var reloadLoanDataList = function (keyword) {
        var showLoadingSpinner = function () {
            $('#divLoanClientData').html('');
            $('#loading').show();
        };
        loanDataService.getClientLoans(keyword.trim(), showLoadingSpinner)
            .then(function (response) {
                $('#divLoanClientData').html(response);
            })
            .catch(function (error) {
                notifier.showError(error);
                $('#divLoanClientData').html('');
            })
            .done(function () {
                $('#loading').hide();
            });
    };

    function domBinding() {
        $(document)
            // search box changed
            .on('change', '#txtSearchBox', function () {
                reloadLoanDataList(this.value);
            })
            // view loan detail
            .on('click', '[name=btnDetail]', function () {
                var clientLoanId = $(this).data('client-loan-id');
                $('#modalWithLoadingSpinner').modal();
                loanService.getLoanDetailModalContent(clientLoanId)
                    .then(function (response) {
                        $('#modalWithLoadingSpinner').find('.modal-content').first().html(response);
                    })
                    .catch(function (error) {
                        $('#modalWithLoadingSpinner').modal('hide');
                        notifier.showError('Failed to load loan detail.');
                    });
            })
            // delete loan
            .on('click', '[name=btnDelete]', function () {
                var countClient = $(this).data('count-client');
                var clientLoanId = $(this).data('client-loan-id');
                var commitmentNumber = $(this).data('commitment-number');
                $('#modalWithLoadingSpinner').modal();
                loanDataService.getDeleteLoanConfirmationModalContent(clientLoanId, commitmentNumber)
                    .then(function (response) {
                        $('#modalWithLoadingSpinner').find('.modal-content').html(response);
                        $('#txtCountClient').val(countClient);
                    })
                    .catch(function () {
                        $('#modalWithLoadingSpinner').modal('hide');
                        notifier.showError('Failed to load delete loan popup.');
                    });
            })
            // delete confirmed
            .on('click', '[name=btnDeleteConfirmed]', function () {
                var clientLoanId = $(this).data('client-loan-id');
                var commitmentNumber = $(this).data('commitment-number');

                loanDataService.deleteLoan(clientLoanId)
                    .then(function (response) {
                        if (response.isSuccess) {
                            $('tr[data-client-loan-id=' + clientLoanId + ']').remove();
                            if ($('tr[name=' + commitmentNumber + ']').length === 0) {
                                $('tr[data-tr-commitment-number=' + commitmentNumber + ']').remove();
                            }
                            notifier.showSuccess('Deleted loan successfully.');
                            $('#modalWithLoadingSpinner').modal('hide');
                        } else {
                            notifier.showMessage('Failed to delete loan.');
                        }
                    })
                    .catch(function (error) {
                        notifier.showMessage('Failed to delete loan.');
                    });
            })
            // select all checkbox handling
            .on('change', '[name= ckAllRegisterInBreeze]', function () {
                var _ckAll = $(this);
                var commitmentNumber = _ckAll.data('commitment-number');
                $('[name=ckRegisterInBreeze][data-commitment-number="' + commitmentNumber + '"]').each(function () { $(this).prop('checked', _ckAll.prop('checked')); });
                handleOnChangeCkSelect();
            })
            // select to register checkbox handling
            .on('change', '[name=ckRegisterInBreeze]', function () {
                handleOnChangeCkSelect();
            })
            // register loan to Breeze
            .on('click', '#btnRegisterInBreeze', function () {
                handleOnChangeCkSelect();
                var registerClientLoanInBreeze = function (clientLoanIds, commitmentNumber) {
                    loanDataService.registerClientLoanInBreeze(clientLoanIds, commitmentNumber)
                        .then(function (registeredLoan) {
                            if (registeredLoan) {
                                handleRegisterSuccess(registeredLoan);
                            }
                        })
                        .catch(function () {
                            notifier.showError('Failed to register loans to Breeze.');
                        });
                };

                var handleRegisterSuccess = function (registeredLoan) {
                    if (registeredLoan.length > 0) {
                        $("#registerResultModal").modal('show');
                        $("#showRegisterResult").html('');
                        $.each(registeredLoan, function (index, item) {
                            $("#showRegisterResult").append($('<p>').addClass("text-success").append($('<span>').append($('<i>').addClass('fa fa-check-circle')))
                                .append($('<span>').css('margin-left', '3px').text('Loan number ' + item.clientLoanNumber + ' is registered with Breeze.')));
                        });
                        reloadLoanDataList('');
                    }
                };

                var selectedClientLoanIds = [];
                var commitmentNumber = [];
                $('[name= ckRegisterInBreeze]')
                    .filter(function (index) { return $(this).prop('checked') === true; })
                    .each(function () {
                        selectedClientLoanIds.push($(this).closest('tr').data('client-loan-id'));
                        commitmentNumber.push($(this).closest('tr').data('commitment-number'));
                    });
                if (selectedClientLoanIds.length > 0) {
                    registerClientLoanInBreeze(selectedClientLoanIds, commitmentNumber);
                }
            })

            // preview loan
            .on('click', '#btnPreviewLoan', function () {
                //handleOnChangeCkSelect();
                var selectedClientLoans = [];
                $('[name^=ckRegisterInBreeze]')
                    .filter(function (index) { return $(this).prop('checked') === true; })
                    .each(function () {
                        selectedClientLoans.push($(this).closest('tr').data('client-loan-id'));
                    });
                loanDataService.generatePreviewLoansFile(selectedClientLoans)
                    .then(function (filename) {
                        window.location.href = `/${$('#ApplicationName').val()}/LoanDataClient/DownloadPreviewLoansFile?filename=${filename}`;
                    })
                    .catch(function (err) {
                        notifier.showError('Failed to export loans to file.');
                    });
            });
    }

    return {
        init: function () {
            reloadLoanDataList('');
            domBinding();
        }()
    };

}();