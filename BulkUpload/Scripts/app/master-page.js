﻿
Object.create = function (o) {
    function F() { }
    F.prototype = o;
    return new F();
}

function inheritPrototype(childObj, parentObj) {
    var copyOfParent = Object.create(parentObj.prototype);
    copyOfParent.constructor = childObj;
    childObj.prototype = copyOfParent;
}

function MasterPage() { }

MasterPage.prototype = {
    constructor: MasterPage,
}