﻿
Array.prototype.sortBy = function (p) {
    return this.slice(0).sort(function (a, b) {
        return (a[p] > b[p]) ? 1 : (a[p] < b[p]) ? -1 : 0;
    });
};

var templateDetailsPage = function () {

    var clientService = new ClientService();
    var templateService = new TemplateService();

    var clientColNameHeaders = [];
    var availableClientColOptions = [];
    var breezeColNameHeaders = [];
    var availableBreezeColOptions = [];
    var mappings = [];

    function init() {
        // block UI
        $("#fileNameShow").html('');
        $("#isShowBtnColMapping").addClass('hide');
        $("#tableColumnMapping").addClass('hide');
        $.when(clientService.getAllClients(), templateService.getBreezeColumnName(), getTemplateDetail())
            .then(function (clients, breezeColumnHeaders, templateDetails) {
                unblockUI();
                handleClientDDL(clients, templateDetails.ClientId);
                breezeColNameHeaders = breezeColumnHeaders;
                handleTemplateDetailData(templateDetails);
            }).catch(function (error) {
                handleErrorException('Failed to get template details.');
                unblockUI();
            });
    }

    function handleErrorException(message) {
        $('#templateDetailContent').html($('<p style="text-align: center;">').addClass("text-danger").append($('<span>').append($('<i>').addClass('fa fa-times-circle')))
            .append($('<span>').css('margin-left', '3px').text(message)));
    }

    function unblockUI() {
        if ($("#hiddenTemplateId").val() !== "0") {
            $("#tableColumnMapping").removeClass('hide');
            $("#isShowBtnColMapping").removeClass('hide');
        }
        $("#modalAjaxLoading").modal('hide');
    }

    function handleClientDDL(options, defaultVal) {
        $('#clientOption').append('<option class="dropdown-item" selected value="0">Select client</option>');
        $.each(options, function (index, option) {
            $('#clientOption').append('<option class="dropdown-item" value="' + option.value + '">' + option.text + '</option>');
        });
        $('#clientOption').val(defaultVal);
    }

    function getTemplateDetail() {
        var df = $.Deferred();
        if (parseInt($("#hiddenTemplateId").val()) === 0) {
            $("#modalAjaxLoading").modal('hide');
            df.resolve({
                clientColumnHeaders: [],
                ClientId: 0,
                templateName: "",
                fileName: "",
                columnMappings: []
            });
        } else {
            $("#modalAjaxLoading").modal('show');
            return templateService.getTemplateById(
                $("#hiddenTemplateId").val());
        }
        return df.promise();
    }

    function handleTemplateDetailData(data) {
        $("#templateName").val(data.TemplateName);
        $("#fileNameShow").val(data.FileName);
        clientColNameHeaders = data.ClientColumnHeaders;
        mappings = data.ColumnMappings || [];
        handleColumnOptions();
    }

    function handleColumnOptions() {
        refreshOptions();
        if (mappings.length > 0)
            renderTemplateMappings(JSON.stringify(clientColNameHeaders), JSON.stringify(breezeColNameHeaders), JSON.stringify(mappings));
    }

    function refreshOptions() {
        if (mappings.length > 0) {
            var alreadySelectedClientCols = mappings.map(item => item.ClientColumnName);
            var alreadySelectedBreezeCols = mappings.map(item => item.LoanDataPropertyName);
            availableClientColOptions = clientColNameHeaders.filter(col => alreadySelectedClientCols.indexOf(col.Value) === -1);
            availableBreezeColOptions = breezeColNameHeaders.filter(col => alreadySelectedBreezeCols.indexOf(col.Value) === -1);
        }
        else {
            availableClientColOptions = clientColNameHeaders;
            availableBreezeColOptions = breezeColNameHeaders;
        }
    }

    function renderTemplateMappings(clientColumns, breezeColumns, mappings) {
        var form = new FormData();
        form.append("clientColumns", clientColumns);
        form.append("breezeColumns", breezeColumns);
        form.append("mappings", mappings);
        templateService.getColumnMappingsView(form)
            .then(res => {
                $("#tableColumnMapping").append(res);
                syncClientColumnOptions();
                syncBreezeColumnOptions();
            });
    }

    function renderNewColumnMappingView(clientColumns, breezeColumns) {
        var form = new FormData();
        form.append("clientColumns", clientColumns);
        form.append("breezeColumns", breezeColumns);
        templateService.getNewColumnMappingView(form)
            .then(res => { $("#tableColumnMapping").append(res); });
    }

    function onUploadFileChange(elm) {
        var error = false;
        clientColNameHeaders = [];
        removeTable();
        resetDataFileUpload();

        // Validator file upload onchange
        var fileUpload = $(elm).get(0);
        var files = fileUpload.files;

        if (!error) {
            var fileData = new FormData();
            for (var i = 0; i < files.length; i++) {
                fileData.append(files[i].name, files[i]);
                $("#fileNameShow").val(files[i].name);
            }
            templateService.getClientHeaderFromFile(fileData, function () { $("#ajaxLoandingReadFile").removeClass('hide'); })
                .then(function (result) {

                    clientColNameHeaders = result;
                    autoMapColumn();

                    $("#tableColumnMapping").removeClass('hide');
                    $("#isShowBtnColMapping").removeClass('hide');

                    handleColumnOptions();
                }).catch(function () {
                    notifier.showError("Error while getting headers from template file.");
                    $("#isShowBtnColMapping").addClass('hide');
                })
                .done(function () {
                    enabledBtnAjaxLoading($("#btnSaveTemplate"));
                    $("#ajaxLoandingReadFile").addClass('hide');
                });

        }
    }

    function autoMapColumn() {
        mappings = [];
        if (clientColNameHeaders.length > 0 && breezeColNameHeaders.length > 0) {
            breezeColNameHeaders.forEach((breeze, index) => {
                var client = clientColNameHeaders.filter(function (client) {
                    return breeze.Text.replace(/\s+/g, '') === client.Name.replace(/\s+/g, '');
                })[0];

                if (client || (!client && breeze.Mandatory)) {
                    var clientColumnName = client ? client.Value : "";
                    mappings.push({
                        ClientColumnName: clientColumnName,
                        BreezeColumnName: breeze.Text,
                        LoanDataPropertyName: breeze.Value
                    });
                }
            });
        }
    }

    function createTemplate(fileData) {
        templateService.create(
            fileData,
            function () {
                disabledBtnAjaxLoading($("#btnSaveTemplate"));
            })
            .then(function (response) {
                if (response.isSuccess) {
                    backToTemplateManagement();
                }
                else {
                    addErrorAttributes($('#templateName'), response.errorMessage);
                }
            })
            .catch(function (error) {
                enabledBtnAjaxLoading($("#btnSaveTemplate"));
                notifier.showError("Error while creating template.");
            })
            .done(function () {
                enabledBtnAjaxLoading($("#btnSaveTemplate"));
            });
    }

    function updateTemplate(fileData) {
        templateService.update(
            fileData,
            function () {
                disabledBtnAjaxLoading($("#btnSaveTemplate"));
            })
            .then(function (response) {
                if (response.isSuccess) {
                    backToTemplateManagement();
                }
                else {
                    addErrorAttributes($('#templateName'), response.errorMessage);
                }
            })
            .catch(function (error) {
                enabledBtnAjaxLoading($("#btnSaveTemplate"));
                notifier.showError("Error while updating template.");
            })
            .done(function () {
                enabledBtnAjaxLoading($("#btnSaveTemplate"));
            });
    }

    function backToTemplateManagement() {
        window.location.href = $('a[name=templates_link]').attr('href');
    }

    function addErrorAttributes(element, mess) {
        if (mess) {
            element.after("<span class='red'>" + mess + "</span>");
        }
        element.addClass("border-error");
        element.focus();
    }

    function removeValidateForFields() {
        $('.border-error').removeClass('border-error');
        $('.red').remove();
    }

    function resetDataFileUpload() {
        $("#fileNameShow").html("");
        $("#isShowBtnColMapping").addClass('hide');
        disabledBtnAjaxLoading($("#btnSaveTemplate"));
        removeValidateForFields();
    }

    function disabledBtnAjaxLoading(elem) {
        elem.prop("disabled", true);
    }

    function enabledBtnAjaxLoading(elem) {
        elem.prop("disabled", false);
    }

    function removeTable() {
        $('#tableColumnMapping tr[name=columnMapping]').remove();
    }

    function addColumnMapping() {
        renderNewColumnMappingView(JSON.stringify(availableClientColOptions), JSON.stringify(availableBreezeColOptions));
    }

    function validateTemplateContent() {
        return new Promise((resolve, reject) => {

            let templateId = parseInt($("#hiddenTemplateId").val());
            let clientId = $('select#clientOption').val();
            let templateName = $('#templateName').val();
           

            let validators = [
                Promise.resolve({ result: clientId !== "0", clientId: clientId }),
                validateTemplateName(templateName, clientId, templateId),
                validateFileUpload(templateId),
                validateColumnMapping()
            ];

            Promise.all(validators)
                .then(validations => {
                    if (!validations[0].result) {
                        addErrorAttributes($('select#clientOption'), "Client is required.");
                    }
                    if (!validations[1].result) {
                        let msg = templateName ? ("The name " + templateName + " is already in use.") : ("Template name is required.");
                        addErrorAttributes($('#templateName'), msg);
                    }
                    if (!validations[2].result) {
                        addErrorAttributes($(".validator-file-upload"), "Template file is required.");
                    }
                    if (!validations[3].result) {
                        addErrorAttributesColumMapping();
                    }
                    if (!validations[0].result || !validations[1].result || !validations[2].result || !validations[3].result) {
                        reject();
                    } else {
                        var formData = new FormData();
                        if (validations[2].file) {
                            formData.append(validations[2].file.name, validations[2].file);
                        }
                        formData.append('templateId', templateId);
                        formData.append('templateName', validations[1].templateName);
                        formData.append('clientId', validations[0].clientId);
                        formData.append('columnMappings', JSON.stringify(validations[3].mappings));
                        resolve(formData);
                    }
                });

        });
    }

    function addErrorAttributesColumMapping() {

        $('tr[name=columnMapping]').each(function (index, value) {
            var _this = $(this);
            var client = _this.find('select[name=ClientColumnName]');
            if (!client.val()) {
                addErrorAttributes($(client));
            }

            var breeze = _this.find('select[name=LoanDataPropertyName]');
            if (!breeze.val()) {
                addErrorAttributes($(breeze));
            }
        });
    }

    function validateTemplateName(tplName, clientId, tplId) {
        return new Promise((resolve, reject) => {
            if (!tplName) {
                resolve({ result: false, templateName: tplName });
                return;
            }

            // temporary resolve this because this is depending on validateClientId
            if (clientId === "0") {
                resolve({ result: true, templateName: tplName });
                return;
            }

            let obj = {
                newTemplateName: tplName,
                fromClientid: clientId,
                templateId: tplId
            };
            templateService.checkExistTemplateName(obj).then(function (res) { resolve({ result: res, templateName: tplName }); });

        });
    }

    function validateFileUpload(templateId) {
        return new Promise((resolve, reject) => {

            let fileUpload = $("#clientFileUpload").get(0);
            let files = fileUpload.files;

            if (templateId !== 0 || files.length > 0) {
                resolve({ result: true, file: files[0] });
            }
            else {
                resolve({ result: false, file: null });
            }
        });
    }

    function validateColumnMapping() {
        return new Promise((resolve, reject) => {

            var valid = true;
            var mappings = [];

            $('tr[name=columnMapping]').each(function (index, value) {
                var _this = $(this);
                var client = _this.find('select[name=ClientColumnName]');
                var breeze = _this.find('select[name=LoanDataPropertyName]');
                if (client.val() && breeze.val()) {
                    var clientCol = clientColNameHeaders.find(item => client.val() === item.Value);
                    var breezeCol = breezeColNameHeaders.find(item => breeze.val() === item.Value);
                    if (clientCol && breezeCol) {
                        mappings.push({
                            ClientColumnName: clientCol.Value,
                            BreezeColumnName: breezeCol.Text,
                            LoanDataPropertyName: breezeCol.Value,
                            ClientColumnDisplayOrder: clientCol.DisplayOrder
                        });
                    }
                } else {
                    valid = false;
                }

            });

            resolve({ result: valid, mappings: mappings });

        });
    }

    function saveTemplate() {
        // remove red box && error message
        removeValidateForFields();
        validateTemplateContent()
            .then((formData) => {
            const templateId = formData.get('templateId');
            // check create or update
            if (templateId === "0") {
                createTemplate(formData);
            } else {
                updateTemplate(formData);
            }
            })
            .catch(error => { });
    }

    function showFileName() {
        $("#clientFileUpload").trigger('click');
    }

    function onClientColumnChange(elm) {
        var oldValue = $(elm).data('old-val');
        if (oldValue) {
            availableClientColOptions.push(clientColNameHeaders.find(item => item.Value === oldValue));
        }
        var newVal = $(elm).val();
        availableClientColOptions = availableClientColOptions.filter(item => item.Value !== newVal);
        $(elm).data('old-val', newVal);
        syncClientColumnOptions();
    }

    function syncClientColumnOptions() {
        $('[name=ClientColumnName]').each((index, item) => {
            let currentVal = $(item).val();
            $(item).html('');
            let options = availableClientColOptions.map(x => x);
            options.push(clientColNameHeaders.find(item => item.Value === currentVal));
            options.splice(0, 0, { Text: "", Value: "" });
            options.forEach(col => {
                if (col) {
                    var option = $('<option>').val(col.Value).text(col.Name);
                    $(item).append(option);
                }
            });
            $(item).val(currentVal);
        });
    }

    function onBreezeColumnChange(elm) {
        var oldValue = $(elm).data('old-val');
        if (oldValue) {
            availableBreezeColOptions.push(breezeColNameHeaders.find(item => item.Value === oldValue));
        }
        var newVal = $(elm).val();
        availableBreezeColOptions = availableBreezeColOptions.filter(item => item.Value !== newVal);
        $(elm).data('old-val', newVal);
        syncBreezeColumnOptions();
    }

    function syncBreezeColumnOptions() {
        $('[name=LoanDataPropertyName]').each((index, item) => {
            let currentVal = $(item).val();
            $(item).html('');
            let options = availableBreezeColOptions.map(x => x);
            options.push(breezeColNameHeaders.find(item => item.Value === currentVal));
            options.splice(0, 0, { Text: "", Value: "" });
            options.forEach(col => {
                var option = $('<option>').val(col.Value).text(col.Text);
                $(item).append(option);
            });
            $(item).val(currentVal);
        });
    }

    function removeMapping(elm) {
        var row = $(elm).closest('tr');

        var clientCol = row.find('[name=ClientColumnName]').val();
        if (clientCol) {
            availableClientColOptions.push(clientColNameHeaders.find(item => item.Value === clientCol));
            syncClientColumnOptions();
        }

        var breezeCol = row.find('[name=LoanDataPropertyName]').val();
        if (breezeCol) {
            availableBreezeColOptions.push(breezeColNameHeaders.find(item => item.Value === breezeCol));
            syncBreezeColumnOptions();
        }

        row.remove();
    }

    return {
        init: init,
        addColumnMapping: addColumnMapping,
        saveTemplate: saveTemplate,
        onClientColumnChange: onClientColumnChange,
        onBreezeColumnChange: onBreezeColumnChange,
        onUploadFileChange: onUploadFileChange,
        removeMapping: removeMapping,
        showFileName: showFileName,
        backToTemplateManagement: backToTemplateManagement
    };

};

var ThePage = templateDetailsPage();
ThePage.init();