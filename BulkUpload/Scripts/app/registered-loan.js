﻿
var registeredLoansPage = function () {
    var registeredLoanService = new RegisteredLoanService();
    var loanService = new LoanService();

    function getAllRegisteredLoan() {
        $('#lstTemplate').html('');
        $('#loading').append($('<i>').addClass('fa fa-spinner fa-spin').css('font-size', '60px'));

        registeredLoanService.getAllRegisteredLoan()
            .then(function (response) {
                $('#lstRegisteredLoan').html(response);
                $(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                });
                $('#loading').html('');
            })
            .catch(function (error) {
                notifier.showError(error);
            });
    }

    function handleOnChangeCkSelect() {
        if ($('[name^= chkExportLoan_]').filter(function () { return $(this).prop('checked') === true; }).length > 0) {
            $('[name=btnExportLoan]').removeAttr('disabled');
        } else {
            $('[name=btnExportLoan]').attr('disabled', 'disabled');
        }
    }

    function domBinding() {
        $(document)
            .on('click', '[name=btnDetail]', function () {
                var clientLoanId = $(this).data('client-loan-id');
                $('#modalWithLoadingSpinner').modal();
                loanService.getRegisteredLoanDetailModalContent(clientLoanId)
                    .then(function (response) {
                        $('#modalWithLoadingSpinner').find('.modal-content').first().html(response);
                    })
                    .catch(function (error) {
                        $('#modalWithLoadingSpinner').modal('hide');
                        notifier.showError('Failed to load loan detail.');
                    });
            })
            // select all checkbox handling
            .on('change', '[name=chkAllExportLoan]', function () {
                var _ckAll = $(this);
                $('[name^=chkExportLoan_]').each(function () { $(this).prop('checked', _ckAll.prop('checked')); });
                handleOnChangeCkSelect();
            })
            // select to register checkbox handling
            .on('change', '[name^= chkExportLoan_]', function () {
                handleOnChangeCkSelect();
            })
            .on('click', '[name=btnExportLoan]', function () {
                var selectedClientLoans = [];
                $('[name^=chkExportLoan_]')
                    .filter(function (index) { return $(this).prop('checked') === true; })
                    .each(function () {
                        selectedClientLoans.push($(this).data('client-loan-id'));
                    });

                registeredLoanService.exportLoansToFile(selectedClientLoans)
                    .then(function (filename) {
                        registeredLoanService.downloadExportedFile(filename);
                    })
                    .catch(function (err) {
                        notifier.showError('Failed to export loans to file.');
                    });
            })
            ;
    }

    return {
        init: function () {
            getAllRegisteredLoan();
            domBinding();
        }()
    };

}();