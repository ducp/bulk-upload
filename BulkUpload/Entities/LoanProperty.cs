﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BulkUpload.Entities
{
    [Table("LoanProperty")]
    public partial class LoanProperty
    {

        [Key]
        public int LoanPropertyId { get; set; }

        public int ClientLoanId { get; set; }
        [ForeignKey("ClientLoanId")]
        public virtual ClientLoan ClientLoan { get; set; }

        public string PropertyName { get; set; }

        public string Value { get; set; }
        
    }
}