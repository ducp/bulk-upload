﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BulkUpload.Entities
{
    [Table("ColumnMapping")]
    public partial class ColumnMapping
    {
        [Key]
        public int ColumnMappingId { get; set; }

        public int TemplateId { get; set; }

        public string ClientColumnName { get; set; }

        public string BreezeColumnName { get; set; }

        public string LoanDataPropertyName { get; set; }

        public int ClientColumnDisplayOrder { get; set; }

        [ForeignKey("TemplateId")]
        public virtual Template Template { get; set; }
    }
}