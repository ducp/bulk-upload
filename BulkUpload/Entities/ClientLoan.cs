﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BulkUpload.Entities
{
    [Table("ClientLoan")]
    public partial class ClientLoan
    {
        public ClientLoan()
        {
            LoanProperties = new HashSet<LoanProperty>();
        }

        [Key]
        public int ClientLoanId { get; set; }

        public int ImportSessionId { get; set; }
        [ForeignKey("ImportSessionId")]
        public virtual ImportSession ImportSession { get; set; }

        public DateTime ImportDate { get; set; }

        public Nullable<int> LineNumber { get; set; }

        public bool IsDeleted { get; set; }

        public virtual ICollection<LoanProperty> LoanProperties { get; set; }
        
        public virtual RegisteredLoan RegisteredLoan { get; set; }
    }
}