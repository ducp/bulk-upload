using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BulkUpload.Entities
{
    [Table("Client")]
    public partial class Client
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Client()
        { }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public int ClientId { get; set; }

        [StringLength(50)]
        [Display(Name = "Client Name")]
        public string Name { get; set; }
    }
}
