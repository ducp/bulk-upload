using System.Data.Entity;

namespace BulkUpload.Entities
{
    public class BulkDbContext : DbContext
    {
        public BulkDbContext()
            : base("name=BulkDbContext")
        {
        }

        public virtual DbSet<Client> Clients { get; set; }

        public virtual DbSet<Template> Templates { get; set; }

        public virtual DbSet<ClientLoan> ClientLoans { get; set; }

        public virtual DbSet<LoanData> LoanData { get; set; }

        public virtual DbSet<ColumnMapping> ColumnMappings { get; set; }

        public virtual DbSet<ImportSession> ImportSessions { get; set; }

        public virtual DbSet<RegisteredLoan> RegisteredLoans { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ClientLoan>()
            .HasOptional(a => a.RegisteredLoan)
            .WithRequired(ab => ab.ClientLoan);
        }
    }

    public class InsidePlazaDbContext : DbContext
    {
        public InsidePlazaDbContext()
            : base("name=InsidePlazaDbContext")
        {
        }

        public virtual DbSet<Client> Clients { get; set; }

    }
}
