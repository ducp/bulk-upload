﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BulkUpload.Entities
{
    [Table("RegisteredLoan")]
    public partial class RegisteredLoan
    {
        public RegisteredLoan()
        {
        }

        // add index to RegisterLoan to make sure ClientLoanId is unique in this table
        [Key, ForeignKey("ClientLoan")]
        public int ClientLoanId { get; set; }

        [Display(Name = "Breeze Loan Id")]
        public string BreezeLoanId { get; set; }

        [Display(Name = "Registered Date")]
        public DateTime RegisteredDate { get; set; }

        public virtual ClientLoan ClientLoan { get; set; }

    }
}