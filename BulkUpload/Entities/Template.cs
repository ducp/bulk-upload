using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BulkUpload.Entities
{

    [Table("Template")]
    public partial class Template
    {
        public Template()
        {
            ColumnMappings = new HashSet<ColumnMapping>();
        }

        [Key]
        public int TemplateId { get; set; }

        [Display(Name = "Client Id")]
        public int ClientId { get; set; }

        [Required]
        [StringLength(200)]
        [Display(Name ="Template Name")]
        public string TemplateName { get; set; }

        [Display(Name = "Template File Name")]
        public string UploadFile { get; set; }

        public string HardFile { get; set; }

        public DateTime? CreatedDate { get; set; }

        public bool IsDelete { get; set; }

        //[ForeignKey("ClientId")]
        //public virtual Client Client { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [Display(Name = "Column Mappings")]
        public virtual ICollection<ColumnMapping> ColumnMappings { get; set; }
    }
}
