﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BulkUpload.Entities
{
    [Table("ImportSession")]
    public partial class ImportSession
    {
        public ImportSession()
        {
            ClientLoans = new HashSet<ClientLoan>();
        }

        [Key]
        public int ImportSessionId { get; set; }

        public int ClientId { get; set; }
        //[ForeignKey("ClientId")]
        //public virtual Client Client { get; set; }

        public int TemplateId { get; set; }
        [ForeignKey("TemplateId")]
        public virtual Template Template { get; set; }

        public string UploadFile { get; set; }

        public string HardFile { get; set; }

        public virtual ICollection<ClientLoan> ClientLoans { get; set; }
    }
}