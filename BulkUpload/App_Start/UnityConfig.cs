using BulkUpload.Entities;
using BulkUpload.Helpers;
using BulkUpload.Repository;
using BulkUpload.Services;
using System;
using System.Data.Entity;
using System.IO;
using System.Web;
using System.Web.Mvc;
using Unity;
using Unity.AspNet.Mvc;
using Unity.Injection;
using Unity.Lifetime;

namespace BulkUpload
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below.
            // Make sure to add a Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your type's mappings here.
            // container.RegisterType<IProductRepository, ProductRepository>();
            //container.RegisterType<IClientService, ClientService>();
            container.RegisterType<IClientService, PlazaClientService>();
            //container.RegisterType<IClientService, BreezeClientService>();
            container.RegisterType<IColumnMappingService, ColumnMappingService>();
            container.RegisterType<IClientLoanService, ClientLoanService>();
            container.RegisterType<ILoanService, LoanService>();
            container.RegisterType<ITemplateService, TemplateService>();
            container.RegisterType<IRegisteredLoanService, RegisteredLoanService>();
            var filePath = System.IO.Path.Combine(ConfigurationProvider.GetValue<string>(SupportedConfiguration.BreezePropertiesFileLocation), "BreezeProperty.json");
            container.RegisterType<IBreezePropertyService, ConfigurableBreezePropertyService>(new InjectionConstructor(filePath));
            container.RegisterType<IConfigurableBreezePropertyService, ConfigurableBreezePropertyService>(new InjectionConstructor(filePath));
            container.RegisterType<IBreezeLoanService, BreezeLoanService>();
            //container.RegisterType<IUserService, BreezeUserService>();
            container.RegisterType<IUserService, LocalUserService>();

            // inject db contexts
            container.RegisterType<DbContext, BulkDbContext>("BulkUploadDbContext", new ContainerControlledLifetimeManager());
            container.RegisterType<DbContext, InsidePlazaDbContext>("InsidePlazaDbContext", new ContainerControlledLifetimeManager());

            // inject repositories for Bulk Upload database
            var bulkUploadDbContext = container.Resolve<BulkDbContext>("BulkUploadDbContext");
            container.RegisterType<IColumnMappingRepository, ColumnMappingRepository>(new InjectionConstructor(bulkUploadDbContext));
            container.RegisterType<IClientLoanRepository, ClientLoanRepository>(new InjectionConstructor(bulkUploadDbContext));
            container.RegisterType<ITemplateRepository, TemplateRepository>(new InjectionConstructor(bulkUploadDbContext));
            container.RegisterType<IImportSessionRepository, ImportSessionRepository>(new InjectionConstructor(bulkUploadDbContext));
            container.RegisterType<IClientLoanRepository, ClientLoanRepository>(new InjectionConstructor(bulkUploadDbContext));
            container.RegisterType<IRegisteredLoanRepository, RegisteredLoanRepository>(new InjectionConstructor(bulkUploadDbContext));
            container.RegisterType<IClientRepository, ClientRepository>(new InjectionConstructor(bulkUploadDbContext));

            // inject repositories for Inside Plaza database
            //var insidePlazaDbContext = container.Resolve<InsidePlazaDbContext>("InsidePlazaDbContext");
            //container.RegisterType<IClientRepository, ClientRepository>(new InjectionConstructor(insidePlazaDbContext));

            //container.RegisterType<FileReader, CsvReader>(".csv", new ContainerControlledLifetimeManager());
            container.RegisterType<FileReader, ExcelReader>(".xlsx", new ContainerControlledLifetimeManager());
            container.RegisterType<FileReader, ExcelReader>(".xls", new ContainerControlledLifetimeManager());

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

        }
    }
}