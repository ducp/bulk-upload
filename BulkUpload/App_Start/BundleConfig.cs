﻿using System.Web;
using System.Web.Optimization;

namespace BulkUpload
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                       "~/Scripts/bootstrap-notify.js"
                      ));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/Site.css",
                      "~/Content/css/notify-metro.css",
                      "~/Content/font-awesome.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/services").Include(
                        "~/Scripts/services/common.js",
                        "~/Scripts/services/client-service.js",
                        "~/Scripts/services/template-service.js",
                        "~/Scripts/services/commit-loan-service.js",
                        "~/Scripts/services/loan-service.js",
                        "~/Scripts/services/loan-data-service.js",
                        "~/Scripts/services/breeze-property-service.js",
                        "~/Scripts/services/registered-loan-service.js"));

            bundles.Add(new ScriptBundle("~/bundles/master-page").Include(
                        "~/Scripts/app/master-page.js"));

            bundles.Add(new ScriptBundle("~/bundles/templates").Include(
                      "~/Scripts/app/templates.js"));

            bundles.Add(new ScriptBundle("~/bundles/loan-data").Include(
                      "~/Scripts/app/loan-data.js"));

            bundles.Add(new ScriptBundle("~/bundles/template-detail").Include(
                      "~/Scripts/app/template-detail.js"));

            bundles.Add(new ScriptBundle("~/bundles/import-loan").Include(
                      "~/Scripts/app/import-loan.js"));

            bundles.Add(new ScriptBundle("~/bundles/template-create").Include(
                      "~/Scripts/app/template-create.js"));

            bundles.Add(new ScriptBundle("~/bundles/breeze-property-management").Include(
                      "~/Scripts/app/breeze-property-management.js"));

            bundles.Add(new ScriptBundle("~/bundles/registered-loan").Include(
                      "~/Scripts/app/registered-loan.js"));

#if !DEBUG
            BundleTable.EnableOptimizations = true;
#endif

        }
    }
}
