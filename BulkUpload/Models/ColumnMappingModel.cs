﻿using BulkUpload.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BulkUpload.Models
{
    public class ColumnMappingModel
    {
        [Key]
        public int Id { get; set; }

        public int TemplateId { get; set; }

        public string ClientName { get; set; }

        public int ClientId { get; set; }

        public string TemplateName { get; set; }

        public string ClientColumnName { get; set; } 

        public string BreezeColumnName { get; set; }

        public string LoanDataPropertyName { get; set; }

        public List<ColumnMapping> ColumnMappings { get; set; }

        public HttpPostedFile Files { get; set; }

        [ForeignKey("TemplateId")]
        public virtual Template Template { get; set; }
    }
}