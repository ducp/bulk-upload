﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Web;
using BulkUpload.Helpers;
using System.Text.RegularExpressions;
using System.Globalization;

namespace BulkUpload.Models
{
    public class BreezeProperty
    {
        [Display(Name = "Field Name")]
        [JsonProperty("propertyName")]
        public string PropertyName { get; set; }

        [Display(Name = "Display Name")]
        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [Display(Name = "Data Type")]
        [JsonProperty("dataType")]
        public string DataType { get; set; }

        [Display(Name = "Max String Length")]
        [JsonProperty("maxStringLength")]
        public int? MaxStringLength { get; set; }

        [Display(Name = "Required")]
        [JsonProperty("mandatory")]
        public bool Mandatory { get; set; }

        [Display(Name = "Display Order")]
        [JsonProperty("order")]
        public int Order { get; set; }

        [Display(Name = "Options")]
        [JsonProperty("options")]
        public List<string> Options { get; set; }

        [Display(Name = "Regrex Pattern")]
        [JsonProperty("regrexPattern")]
        public string RegrexPattern { get; set; }

        [Display(Name = "Export Using Name")]
        [JsonProperty("exportUsingName")]
        public string ExportUsingName { get; set; }

        [Display(Name = "Is Currency")]
        [JsonProperty("isCurrency")]
        public bool IsCurrency { get; set; }

        [Display(Name = "Round Number Down")]
        [JsonProperty("roundNumberDown")]
        public bool RoundNumberDown { get; set; }

        public BreezeFieldHandler BreezeStringHandler(string stringValue)
        {

            Type type = ConfigurationProvider.SupportedDataTypes[DataType];

            if (type == typeof(DateTime))
            {
                return new DateTypeHandler(stringValue);
            }
            else if (type == typeof(Enum))
            {
                return new EnumTypeHandler(stringValue, Options);
            }
            else if (type == typeof(string))
            {
                return new StringTypeHandler(stringValue, MaxStringLength, RegrexPattern);
            }
            else
            {
                return new PrimitiveTypeHandler(stringValue, type, Mandatory, IsCurrency, RoundNumberDown);
            }
        }

    }
}