﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BulkUpload.Models
{
    public class ImportErrorModel
    {
        public int LineNumber { get; set; }
        public string ForColumn { get; set; }
        public string Messge { get; set; }
    }
}