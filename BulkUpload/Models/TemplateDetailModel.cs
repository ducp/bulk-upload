﻿using BulkUpload.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BulkUpload.Models
{
    public class TemplateDetailModel
    {
        public int TemplateId { get; set; }
        public string TemplateName { get; set; }
        public string FileName { get; set; }
        public int ClientId { get; set; }
        public virtual ICollection<ColumnMapping> ColumnMappings { get; set; }
        public List<CellModel> ClientColumnHeaders { get; set; }
    }
}