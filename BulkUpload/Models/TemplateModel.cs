﻿using BulkUpload.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BulkUpload.Models
{
    public class TemplateModel
    {
        public int TemplateId { get; set; }
        public string TemplateName { get; set; }
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public string UploadFile { get; set; }
        public virtual ICollection<ColumnMapping> ColumnMappings { get; set; }
    }
}