﻿using BulkUpload.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BulkUpload.Models
{
    public class ExtractedClientLoanModel
    {
        public int LineNumber { get; set; }
        public LoanData LoanData { get; set; }
        public List<ImportErrorModel> Errors { get; set; }
        public bool AllowImport => !Errors.Any();
        public Dictionary<string, string> PropertiesValueInString { get; set; }
    }
}