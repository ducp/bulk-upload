﻿using BulkUpload.Models;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BulkUpload.Entities
{
    public partial class LoanData
    {
        [Key]
        public int Id { get; set; }

        //[BreezeColumnName("Correspondent Id")]
        //[StringLength(50)]
        //[BreezeMandatory]
        //[FieldOrder(1)]
        //[ExportHeaderName("Correspondent Name")]
        //public string CorrespondentId { get; set; }

        //[BreezeColumnName("Correspondent Name")]
        //[StringLength(50)]
        //[FieldOrder(2)]
        //[ExportHeaderName("Correspondent Name")]
        //public string CorrespondentName { get; set; }

        [BreezeColumnName("Client Loan Number")]
        [StringLength(15)]
        [BreezeMandatory]
        [FieldOrder(1)]
        [ExportHeaderName("Client Loan Number")]
        public string ClientLoanNumber { get; set; }

        [BreezeColumnName("Commitment Number")]
        [StringLength(20)]
        [BreezeMandatory]
        [FieldOrder(2)]
        [ExportHeaderName("Commitment Number")]
        public string CommitmentNumber { get; set; }

        [BreezeColumnName("Lock Date")]
        [BreezeMandatory]
        [FieldOrder(3)]
        [ExportHeaderName("Lock Date")]
        public DateTime LockDate { get; set; }

        [BreezeColumnName("Lock Term")]
        [BreezeMandatory]
        [FieldOrder(4)]
        [ExportHeaderName("Lock Term")]
        public decimal LockTerm { get; set; }

        [BreezeColumnName("Price")]
        [BreezeMandatory]
        [FieldOrder(5)]
        [ExportHeaderName("Price")]
        public decimal Price { get; set; }

        [BreezeColumnName("Program")]
        [StringLength(20)]
        [BreezeMandatory]
        [FieldOrder(6)]
        [ExportHeaderName("Program")]
        public string Program { get; set; }

        [BreezeColumnName("Note Rate")]
        [BreezeMandatory]
        [FieldOrder(7)]
        [ExportHeaderName("Note Rate")]
        public decimal NoteRate { get; set; }

        [BreezeColumnName("Loan Amount")]
        [BreezeMandatory]
        [FieldOrder(8)]
        [ExportHeaderName("Loan Amount")]
        public decimal LoanAmount { get; set; }

        [BreezeColumnName("Borrower First Name")]
        [StringLength(20)]
        [BreezeMandatory]
        [FieldOrder(9)]
        [ExportHeaderName("Borrower First Name")]
        public string BorrowerFirstName { get; set; }

        [BreezeColumnName("Borrower Last Name")]
        [StringLength(20)]
        [BreezeMandatory]
        [FieldOrder(10)]
        [ExportHeaderName("Borrower Last Name")]
        public string BorrowerLastName { get; set; }

        [BreezeColumnName("Property Type")]
        [StringLength(20)]
        [FieldOrder(11)]
        [ExportHeaderName("Property Type")]
        public string PropertyType { get; set; }

        [BreezeColumnName("Doc Type")]
        [StringLength(20)]
        [BreezeMandatory]
        [FieldOrder(12)]
        [ExportHeaderName("Doc Type")]
        public string DocType { get; set; }

        //[BreezeColumnName("LTV")]
        //[BreezeMandatory]
        //[FieldOrder(13)]
        //[ExportHeaderName("LTV")]
        //public decimal LTV { get; set; }

        [BreezeColumnName("CLTV")]
        [BreezeMandatory]
        [FieldOrder(14)]
        [ExportHeaderName("CLTV")]
        public decimal CLTV { get; set; }

        [BreezeColumnName("Owner Occupied")]
        [StringLength(50)]
        [FieldOrder(15)]
        [ExportHeaderName("Owner Occupied")]
        public string OwnerOccupied { get; set; }

        [BreezeColumnName("Purpose")]
        [StringLength(20)]
        [FieldOrder(16)]
        [ExportHeaderName("Purpose")]
        public string Purpose { get; set; }

        [BreezeColumnName("CashOut")]
        [FieldOrder(17)]
        [ExportHeaderName("CashOut")]
        public decimal CashOut { get; set; }

        [BreezeColumnName("FICO")]
        [FieldOrder(18)]
        [ExportHeaderName("FICO")]
        public decimal FICO { get; set; }

        [BreezeColumnName("ULI")]
        [StringLength(50)]
        [FieldOrder(19)]
        [ExportHeaderName("ULI")]
        public string ULI { get; set; }

        [BreezeColumnName("Warehouse Bank")]
        [StringLength(20)]
        [FieldOrder(20)]
        [ExportHeaderName("Warehouse Bank")]
        public string WarehouseBank { get; set; }

        [BreezeColumnName("Appraisal Value")]
        [FieldOrder(21)]
        [ExportHeaderName("Appraisal Value")]
        public decimal AppraisalValue { get; set; }

        [BreezeColumnName("Purchase Price")]
        [FieldOrder(22)]
        [ExportHeaderName("Purchase Price")]
        public decimal PurchasePrice { get; set; }

        [BreezeColumnName("Address")]
        [StringLength(200)]
        [BreezeMandatory]
        [FieldOrder(23)]
        [ExportHeaderName("Address")]
        public string Address { get; set; }

        [BreezeColumnName("City")]
        [StringLength(50)]
        [BreezeMandatory]
        [FieldOrder(24)]
        [ExportHeaderName("City")]
        public string City { get; set; }

        [BreezeColumnName("State")]
        [StringLength(20)]
        [BreezeMandatory]
        [FieldOrder(25)]
        [ExportHeaderName("State")]
        public string State { get; set; }

        [BreezeColumnName("Zip")]
        [StringLength(20)]
        [BreezeMandatory]
        [FieldOrder(26)]
        [ExportHeaderName("Zip")]
        public string Zip { get; set; }
    }

    public class FieldOrderAttribute : Attribute
    {
        public int Order { get; private set; }

        public FieldOrderAttribute(int order)
        {
            Order = order;
        }
    }
}