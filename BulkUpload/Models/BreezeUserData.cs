﻿using BulkUpload.Entities;
using System.Collections.Generic;

namespace BulkUpload.Models
{
    public class BreezeUserData
    {
        public string Username { get; set; }
        public List<Client> Clients { get; set; }
        public List<string> Roles { get; set; }
    }
}