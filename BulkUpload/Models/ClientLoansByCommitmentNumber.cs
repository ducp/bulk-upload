﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BulkUpload.Models
{
    public class ClientLoansByCommitmentNumber
    {
        public string CommitmentNumber { get; set; }
        public List<ClientLoanModel> ClientLoans { get; set; }
    }
}