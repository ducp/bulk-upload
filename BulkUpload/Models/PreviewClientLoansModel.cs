﻿using BulkUpload.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace BulkUpload.Models
{
    public class PreviewClientLoansModel
    {
        [JsonProperty("header")]
        public List<PreviewLoanHeaderModel> Header { get; set; }
        [JsonProperty("data")]
        public List<ClientLoanDataModel> Data { get; set; }
    }
}