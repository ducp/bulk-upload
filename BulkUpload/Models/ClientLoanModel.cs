﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BulkUpload.Models
{
    public class ClientLoanModel
    {
        public int Id { get; set; }

        public string ClientName { get; set; }

        public string TemplateName { get; set; }

        public DateTime ImportDate { get; set; }

        public Nullable<int> LineNumber { get; set; }

        public string LoanNumber { get; set; }

        public string BorrowerFirstName { get; set; }

        public string BorrowerLastName { get; set; }

        public string PropertyAddress { get; set; }
        public string CommitmentNumber { get; set; }
        
    }
}