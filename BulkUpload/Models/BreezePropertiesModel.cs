﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BulkUpload.Models
{
    public class BreezePropertiesModel
    {
        [JsonProperty("properties")]
        public List<BreezeProperty> Properties { get; set; }
    }
}