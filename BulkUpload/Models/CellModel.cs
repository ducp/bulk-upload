﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BulkUpload.Models
{
    public class CellModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string CellReference { get; set; }
        public int DisplayOrder { get; set; }
    }

    public class RowModel
    {
        public string LineNumber { get; set; }
        public List<CellModel> Cells { get; set; }
    }
}