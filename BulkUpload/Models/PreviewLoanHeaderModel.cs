﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BulkUpload.Models
{
    public class PreviewLoanHeaderModel
    {
        [JsonProperty("breezeColumnName")]
        public string BreezeColumnName { get; set; }
        [JsonProperty("isMandatory")]
        public bool IsMandatory { get; set; }
        [JsonProperty("clientFileColumnName")]
        public string ClientFileColumnName { get; set; }
        [JsonProperty("tooltipTitle")]
        public string TooltipTitle { get; set; }
        [JsonProperty("displayOrder")]
        public int DisplayOrder { get; set; }
    }
}