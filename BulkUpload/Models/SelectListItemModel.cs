﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BulkUpload.Models
{
    public class SelectListItemModel
    {
        public bool Mandatory { get; set; }
        
        public string Text { get; set; }
        
        public string Value { get; set; }
    }
}