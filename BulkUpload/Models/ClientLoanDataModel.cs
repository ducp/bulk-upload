﻿using BulkUpload.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using BulkUpload.Helpers;

namespace BulkUpload.Models
{
    public class ClientLoanDataModel
    {
        [JsonProperty("lineNumber")]
        public int LineNumber { get; set; }

        [JsonProperty("loanProps")]
        public List<LoanProp> LoanProps { get; set; }

        [JsonProperty("invalidDataRow")]
        public bool InvalidDataRow => LoanProps.Any(x => x.MissingValue || x.InvalidValue);

        public void CheckDuplicated(List<ClientLoan> existingClientLoans)
        {
            string loanNumber = LoanProps.GetClientLoanNumberFromProperties();
            string address = LoanProps.GetPropertyAddressFromProperties();
            if (!string.IsNullOrWhiteSpace(loanNumber) && !string.IsNullOrWhiteSpace(address))
            {
                existingClientLoans.ForEach(existingLoan =>
                {
                    var existingLoanNumber = existingLoan.LoanProperties.ToList().GetClientLoanNumberFromProperties();
                    var existingAddress = existingLoan.LoanProperties.ToList().GetPropertyAddressFromProperties();
                    if (existingLoanNumber.ToLower().Equals(loanNumber.ToLower()) || existingAddress.ToLower().Equals(address.ToLower()))
                    {
                        IsDuplicated = true;
                        DuplicatedLoanId = existingLoan.ClientLoanId;
                        DuplicatedLoanImportedDate = existingLoan.ImportDate.ToString("M/d/yyyy");
                        DuplicatedClientLoanNumber = loanNumber;
                        return;
                    }
                });
            }
        }

        [JsonProperty("isDuplicated")]
        public bool IsDuplicated { get; private set; }

        [JsonProperty("duplicatedLoanId")]
        public int DuplicatedLoanId { get; private set; }

        [JsonProperty("duplicatedLoanImportedDate")]
        public string DuplicatedLoanImportedDate { get; private set; }

        [JsonProperty("duplicatedClientLoanNumber")]
        public string DuplicatedClientLoanNumber { get; private set; }
    }
}