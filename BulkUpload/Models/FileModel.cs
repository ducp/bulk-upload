﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BulkUpload.Models
{
    public class FileModel
    {
        public string Filename { get; set; }
        public string ContentType { get; set; }
        public byte[] Data { get; set; }
    }
}