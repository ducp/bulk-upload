﻿using BulkUpload.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BulkUpload.Models
{
    public class ImportSessionModel
    {
        [JsonProperty("importSessionId")]
        public int ImportSessionId { get; set; }
        [JsonProperty("importResults")]
        public List<ImportClientLoanResult> ImportResults { get; set; }
    }

    public class ImportClientLoanResult
    {
        [JsonProperty("clientLoanNumber")]
        public string ClientLoanNumber { get; set; }
        [JsonProperty("isSuccess")]
        public bool IsSuccess { get; set; }
        [JsonProperty("errorMessage")]
        public string ErrorMessage { get; set; }
        [JsonProperty("lineNumber")]
        public int LineNumber { get; set; }
        [JsonProperty("isOverridden")]
        public bool IsOverridden { get; set; }

    }

}