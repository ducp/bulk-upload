﻿using Newtonsoft.Json;

namespace BulkUpload.Models
{
    public class LoanProp
    {
        [JsonProperty("missingMapping")]
        public bool MissingMapping { get; set; }

        [JsonProperty("stringValue")]
        public string StringValue { get; set; }

        [JsonProperty("breezePropName")]
        public string BreezePropName { get; set; }

        [JsonProperty("breezeColumnName")]
        public string BreezeColumnName { get; set; }

        [JsonProperty("clientColumnName")]
        public string ClientColumnName { get; set; }

        [JsonProperty("isMandatory")]
        public bool IsMandatory { get; set; }

        [JsonProperty("missingValue")]
        public bool MissingValue => IsMandatory && string.IsNullOrWhiteSpace(StringValue);

        [JsonProperty("invalidValue")]
        public bool InvalidValue { get; set; }

        [JsonProperty("displayValue")]
        public string DisplayValue { get; set; }

        [JsonProperty("validateDataErrorMessage")]
        public string ValidateDataErrorMessage { get; set; }

        [JsonProperty("cellReference")]
        public string CellReference { get; set; }

        [JsonProperty("displayOrder")]
        public int DisplayOrder { get; set; }
    }
}