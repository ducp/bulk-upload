﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BulkUpload.Models
{
    public class RegisteredLoanModel
    {
        public int ClientLoanId { get; set; }

        public string LoanNumber { get; set; }

        public string BreezeLoanId { get; set; }

        public DateTime RegisteredDate { get; set; }

        public string Client { get; set; }
    }
}