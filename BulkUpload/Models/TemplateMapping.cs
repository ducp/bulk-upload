﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BulkUpload.Models
{
    public class TemplateMapping
    {
        public string ClientColumnName { get; set; }
        public string BreezeColumnName { get; set; }
        public string LoanDataPropertyName { get; set; }
        public bool IsMandatory { get; set; }
    }
}