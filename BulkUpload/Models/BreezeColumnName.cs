﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BulkUpload.Models
{
    public class BreezeColumnName : Attribute
    {
        public BreezeColumnName(string name)
        {
            Name = name;
        }
        public string Name { get; }
    }

    public class ExportHeaderName : Attribute
    {
        public ExportHeaderName(string name)
        {
            Name = name;
        }
        public string Name { get; }
    }
}