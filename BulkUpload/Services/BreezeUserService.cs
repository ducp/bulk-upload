﻿using BulkUpload.Helpers;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Web.Security;

namespace BulkUpload.Services
{
    public interface IUserService
    {
        bool Login(string username, string password);
    }

    public class BreezeUserService : IUserService
    {
        public bool Login(string username, string password)
        {
            try
            {
                var epicWebServicesUrl = ConfigurationProvider.GetValue<string>(SupportedConfiguration.EpicWebServicesUrl);
                var soapTemplatesLocation = ConfigurationProvider.GetValue<string>(SupportedConfiguration.SOAPTemplatesLocation);
                var epicAppLoginTemplateFilename = ConfigurationProvider.GetValue<string>(SupportedConfiguration.EpicAppLoginSOAPTemplateFilename);
                var templatePath = Path.Combine(soapTemplatesLocation, epicAppLoginTemplateFilename);
                WebServiceHandler epicAppLogin = new EpicAppLogin(epicWebServicesUrl, templatePath, username, password);
                var response = epicAppLogin.InvokeService();
                return response.InnerText.Contains("Success");
            }
            catch (Exception e)
            {
                Log.Instance.Error($"Failed to authenticate user {username}", e);
                return false;
            }
        }
    }

    public class LocalUserService : IUserService
    {
        /// <summary>
        /// by pass for dec local env, cause Epic AppLogin Service does work on VPN only.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool Login(string username, string password)
        {
            return true;
        }
    }

}