﻿using BulkUpload.Entities;
using BulkUpload.Helpers;
using BulkUpload.Repository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BulkUpload.Services
{
    public interface IClientService
    {
        Task<List<Client>> GetAllClients();
        Task<Client> GetById(int id);
    }

    public class ClientService : IClientService
    {
        private readonly IClientRepository _clientRepository;
        public ClientService(IClientRepository clientRepository)
        {
            _clientRepository = clientRepository;
        }

        public async Task<Client> GetById(int id)
        {
            return await _clientRepository.GetById(id);
        }

        public async Task<List<Client>> GetAllClients()
        {
            var clients = await _clientRepository.GetAllClients();
            clients.Add(new Client
            {
                ClientId = 1,
                Name = "Plaza"
            });
            return clients;
        }
    }

    public class PlazaClientService : IClientService
    {
        private readonly string _epicConnectionString;
        private readonly string _epicDB;

        public PlazaClientService()
        {
            _epicConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["EpicConnectionString"].ConnectionString;
            _epicDB = ConfigurationProvider.GetValue<string>(SupportedConfiguration.EpicDB);
        }

        public async Task<List<Client>> GetAllClients()
        {
            var clients = await QueryClientList();
            clients.Add(new Client
            {
                ClientId = 1,
                Name = "Plaza"
            });
            return clients;
        }

        private async Task<List<Client>> QueryClientList()
        {
            var clients = new List<Client>();
            using (SqlConnection connection = new SqlConnection(_epicConnectionString))
            {
                var query = $@"SELECT rolodex_entity.alias2, rolodex_entity.name, rolodex_entity.entityid 
                            FROM {_epicDB}.dbo.rolodex_entity 
                            INNER JOIN {_epicDB}.dbo.rolodex_category_list WITH(nolock) ON rolodex_entity.lenderdatabaseid = rolodex_category_list.entitylenderdatabaseid AND rolodex_entity.entityid = rolodex_category_list.entityid 
                            LEFT OUTER join {_epicDB}.dbo.rolodex_categories WITH(nolock) ON rolodex_categories.categoryid = rolodex_category_list.categoryid 
                            WHERE rolodex_categories.categoryid = 38 AND rolodex_entity.costcenter IN ('NAT','NATU','NATD','NATT','NATQ','NATC','NATS','NATI','NATO','NATN') AND rolodex_entity.delegatedflag=1";

                SqlCommand cmd = new SqlCommand(query, connection);
                connection.Open();
                SqlDataReader reader = await cmd.ExecuteReaderAsync();
                try
                {
                    while (reader.Read())
                    {
                        clients.Add(new Client
                        {
                            ClientId = int.Parse(reader["alias2"].ToString()),
                            Name = reader["name"].ToString()
                        });
                    }
                    return clients;
                }
                catch (Exception e)
                {
                    Log.Instance.Error(e);
                    return new List<Client>();
                }
            }
        }

        public async Task<Client> GetById(int id)
        {
            if (id == 1)
            {
                return new Client
                {
                    ClientId = 1,
                    Name = "Plaza"
                };
            }

            var clients = await GetAllClients();
            return clients.SingleOrDefault(x => x.ClientId == id);
        }

    }

}