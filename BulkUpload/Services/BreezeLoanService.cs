﻿using BulkUpload.Helpers;
using BulkUpload.Models;
using System.Threading.Tasks;

namespace BulkUpload.Services
{
    public interface IBreezeLoanService
    {
        Task<RegisteredLoanModel> RegisterLoanInBreeze(int clientLoanId);
    }

    public class BreezeLoanService : IBreezeLoanService
    {
        public async Task<RegisteredLoanModel> RegisterLoanInBreeze(int clientLoanId)
        {
            return new RegisteredLoanModel
            {
                BreezeLoanId = Utils.RandomString(10),
                ClientLoanId = clientLoanId
            };
        }
    }
}