﻿using BulkUpload.Entities;
using BulkUpload.Helpers;
using BulkUpload.Models;
using BulkUpload.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace BulkUpload.Services
{
    public interface IRegisteredLoanService
    {
        Task<List<RegisteredLoan>> GetAllRegisteredLoan(List<int> clientIds);
        Task<RegisteredLoan> GetByClientLoanId(int clientLoanId);
        //Task<string> ExportLoansToCsvFile(List<int> clientLoanIds, List<BreezeProperty> properties);
        //string GetExportedDataFileLocation(string filename);
    }
    public class RegisteredLoanService : IRegisteredLoanService
    {
        public readonly IRegisteredLoanRepository _registeredLoanRepository;
        public readonly IClientLoanRepository _clientLoanRepository;

        private readonly string _exportFileLocation;

        public RegisteredLoanService(IRegisteredLoanRepository registeredLoanRepository, IClientLoanRepository clientLoanRepository)
        {
            _registeredLoanRepository = registeredLoanRepository;
            _clientLoanRepository = clientLoanRepository;
            _exportFileLocation = ConfigurationProvider.GetValue<string>(SupportedConfiguration.ExportedXLSXRegLoanDataFilesLocation);
        }

        //public string GetExportedDataFileLocation(string filename)
        //{
        //    return Path.Combine(_exportFileLocation, filename);
        //}

        //public async Task<string> ExportLoansToCsvFile(List<int> clientLoanIds, List<BreezeProperty> properties)
        //{
        //    StringBuilder builder = new StringBuilder();

        //    // create header arr
        //    var header = new List<string>();
        //    foreach (var property in properties)
        //    {
        //        var h = property.ExportUsingName;
        //        if (string.IsNullOrEmpty(h))
        //        {
        //            h = property.DisplayName;
        //        }
        //        header.Add(h);
        //    }
        //    builder.AppendLine(string.Join(",", header));

        //    List<ClientLoan> clientLoans = await _clientLoanRepository.GetByIds(clientLoanIds);
        //    // loop to loan and write relevant value
        //    foreach (ClientLoan loan in clientLoans)
        //    {
        //        var fieldData = new List<string>();
        //        foreach (var property in properties)
        //        {
        //            fieldData.Add(loan.LoanProperties?.FirstOrDefault(x => x.PropertyName.ToLower().Equals(property.PropertyName.ToLower()))?.Value ?? string.Empty);
        //        }
        //        builder.AppendLine(string.Join(",", fieldData));
        //    }

        //    string fileName = $"Exported_Loans_{DateTime.Now.ToString("yyyyMdHmsfff")}.csv";
        //    string exportedFile = Path.Combine(_exportFileLocation, fileName);
        //    File.WriteAllText(exportedFile, builder.ToString());

        //    return fileName;
        //}

        public Task<List<RegisteredLoan>> GetAllRegisteredLoan(List<int> clientIds)
        {
            return _registeredLoanRepository.GetAllRegisteredLoan(clientIds);
        }

        public async Task<RegisteredLoan> GetByClientLoanId(int clientLoanId)
        {
            return await _registeredLoanRepository.GetByClientLoanId(clientLoanId);
        }
    }
}