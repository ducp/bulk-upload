﻿using BulkUpload.Entities;
using BulkUpload.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using BulkUpload.Helpers;
using BulkUpload.Models;
using Newtonsoft.Json;
using System.Text;
using System.IO;
using DocumentFormat.OpenXml.Spreadsheet;

namespace BulkUpload.Services
{
    public interface IClientLoanService
    {
        Task<List<ClientLoansByCommitmentNumber>> GetClientLoans(List<Client> clients, string keyword);
        Task<ClientLoan> GetById(int clientLoanId);
        Task<string> GetClientLoanNumberById(int clientLoanId);
        Task DeleteLoan(int clientLoanId);
        Task<List<RegisteredLoanModel>> RegisterBreezeLoan(List<RegisteredLoanModel> data);
        Task<string> GeneratePreviewFile(List<int> clientLoanIds, List<BreezeProperty> properties);
        string GetPreviewDataFileLocation(string filename);
        Task<string> ExportLoansFile(List<int> clientLoanIds, List<BreezeProperty> properties);
        string GetExportedDataFileLocation(string filename);
    }
    public class ClientLoanService : IClientLoanService
    {
        private readonly IClientLoanRepository _clientLoanRepository;
        private readonly string _previewFileLocation;
        private readonly string _exportFileLocation;

        public ClientLoanService(IClientLoanRepository clientLoanRepository)
        {
            _clientLoanRepository = clientLoanRepository;
            _previewFileLocation = ConfigurationProvider.GetValue<string>(SupportedConfiguration.PreviewLoansFilesLocation);
            _exportFileLocation = ConfigurationProvider.GetValue<string>(SupportedConfiguration.ExportedXLSXRegLoanDataFilesLocation);
        }

        public async Task<List<ClientLoansByCommitmentNumber>> GetClientLoans(List<Client> clients, string keyword)
        {
            try
            {
                var clientsLoan = await _clientLoanRepository.GetAllLoanData(clients.Select(x => x.ClientId).ToList(), keyword);

                return clientsLoan.Select(x => new ClientLoanModel
                {
                    Id = x.ClientLoanId,
                    LoanNumber = x.LoanProperties?.FirstOrDefault(p => p.PropertyName == UndeletableProperties.ClientLoanNumber.ToString())?.Value ?? "",
                    TemplateName = x.ImportSession.Template.TemplateName,
                    ClientName = clients.FirstOrDefault(c => c.ClientId == x.ImportSession.ClientId)?.Name ?? string.Empty,
                    LineNumber = x.LineNumber,
                    ImportDate = x.ImportDate,
                    BorrowerFirstName = x.LoanProperties?.FirstOrDefault(p => p.PropertyName == UndeletableProperties.BorrowerFirstName.ToString())?.Value ?? "",
                    BorrowerLastName = x.LoanProperties?.FirstOrDefault(p => p.PropertyName == UndeletableProperties.BorrowerLastName.ToString())?.Value ?? "",
                    PropertyAddress = x.LoanProperties?.FirstOrDefault(p => p.PropertyName == UndeletableProperties.Address.ToString())?.Value ?? "",
                    CommitmentNumber = x.LoanProperties?.FirstOrDefault(p => p.PropertyName == UndeletableProperties.CommitmentNumber.ToString())?.Value ?? "",
                })
                .GroupBy(x => x.CommitmentNumber)
                .Select(x => new ClientLoansByCommitmentNumber
                {
                    CommitmentNumber = x.Key,
                    ClientLoans = x.ToList()
                })
                .ToList();
            }
            catch (Exception ex) { return new List<ClientLoansByCommitmentNumber>(); }
        }

        public async Task<ClientLoan> GetById(int clientLoanId)
        {
            return await _clientLoanRepository.GetById(clientLoanId);
        }

        public async Task<string> GetClientLoanNumberById(int clientLoanId)
        {
            var clientLoan = await _clientLoanRepository.GetById(clientLoanId);
            return clientLoan.LoanProperties.ToList().GetClientLoanNumberFromProperties();
        }

        public async Task DeleteLoan(int clientLoanId)
        {
            await _clientLoanRepository.Delete(clientLoanId);
        }

        public async Task<List<RegisteredLoanModel>> RegisterBreezeLoan(List<RegisteredLoanModel> registeredLoanInBreeze)
        {
            var registeredLoans = new List<RegisteredLoanModel>();
            foreach (var loan in registeredLoanInBreeze)
            {
                var clientLoan = await _clientLoanRepository.AddBreezeLoan(loan.ClientLoanId, new RegisteredLoan
                {
                    BreezeLoanId = loan.BreezeLoanId,
                    RegisteredDate = DateTime.Now,
                });
                registeredLoans.Add(new RegisteredLoanModel
                {
                    BreezeLoanId = clientLoan.RegisteredLoan.BreezeLoanId,
                    ClientLoanId = clientLoan.ClientLoanId,
                    LoanNumber = clientLoan.LoanProperties.ToList().GetClientLoanNumberFromProperties()
                });
            }

            return registeredLoans;
        }

        public async Task<string> GeneratePreviewFile(List<int> clientLoanIds, List<BreezeProperty> properties)
        {
            List<ClientLoan> clientLoans = await _clientLoanRepository.GetByIds(clientLoanIds);
            string fileName = $"Preview_Loans_{DateTime.Now.ToString("yyyyMdHmsfff")}.xlsx";
            string exportedFile = Path.Combine(_previewFileLocation, fileName);
            var result = CreateFile(exportedFile, clientLoans, properties);
            return result ? fileName : string.Empty;
        }

        public string GetPreviewDataFileLocation(string filename)
        {
            return Path.Combine(_previewFileLocation, filename);
        }

        public async Task<string> ExportLoansFile(List<int> clientLoanIds, List<BreezeProperty> properties)
        {
            List<ClientLoan> clientLoans = await _clientLoanRepository.GetByIds(clientLoanIds);
            string fileName = $"Exported_Loans_{DateTime.Now.ToString("yyyyMdHmsfff")}.xlsx";
            string exportedFile = Path.Combine(_exportFileLocation, fileName);
            var result = CreateFile(exportedFile, clientLoans, properties);
            return result ? exportedFile : string.Empty;
        }

        public string GetExportedDataFileLocation(string filename)
        {
            return Path.Combine(_exportFileLocation, filename);
        }

        public bool CreateFile(string outFile, List<ClientLoan> clientLoans, List<BreezeProperty> properties)
        {
            FileWriter writer = new ExcelWriter();
            List<Row> rows = new List<Row>();
            rows.Add(CreateHeaderRowForExcel(writer, properties));
            rows.AddRange(clientLoans.Select(loan => GenerateRowForChildPartDetail(writer, loan, properties)));
            return writer.GenerateFile(outFile, rows);
        }

        private Row CreateHeaderRowForExcel(FileWriter writer, List<BreezeProperty> properties)
        {
            Row workRow = new Row();
            foreach (var prop in properties)
            {
                var h = prop.ExportUsingName;
                if (string.IsNullOrEmpty(h))
                {
                    h = prop.DisplayName;
                }
                workRow.Append(writer.CreateCell(h));
            }
            return workRow;
        }

        private Row GenerateRowForChildPartDetail(FileWriter writer, ClientLoan loan, List<BreezeProperty> properties)
        {
            Row tRow = new Row();
            foreach (var property in properties)
            {
                var val = loan.LoanProperties?.FirstOrDefault(x => x.PropertyName.ToLower().Equals(property.PropertyName.ToLower()))?.Value ?? string.Empty;
                tRow.Append(writer.CreateCell(property.BreezeStringHandler(val).GetStringValue()));
            }
            return tRow;
        }


    }
}