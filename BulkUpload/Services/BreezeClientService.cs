﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using BulkUpload.Entities;
using Newtonsoft.Json;

namespace BulkUpload.Services
{

    public class BreezeClientService : IClientService
    {
        public async Task<List<Client>> GetAllClients()
        {
            var cookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (cookie != null)
            {
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);
                var data = JsonConvert.DeserializeObject<BulkUpload.Models.BreezeUserData>(ticket.UserData);
                return data.Clients;
            }
            return new List<Client>();
        }

        public async Task<Client> GetById(int id)
        {
            var clients = await GetAllClients();
            return clients.FirstOrDefault(x => x.ClientId == id);
        }
    }
}