﻿using BulkUpload.Entities;
using BulkUpload.Helpers;
using BulkUpload.Models;
using BulkUpload.Repository;
using DocumentFormat.OpenXml.Spreadsheet;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BulkUpload.Services
{
    public interface ILoanService
    {
        Task<PreviewClientLoansModel> ExtractClientLoansFromPostedFile(int clientId, int templateId);
        Task<ImportSessionModel> ImportLoans(int importSessionId, int clientId, int templateId, string loansString, HttpPostedFileBase file);
        Task<string> GenerateErrorLoansFile(List<ClientLoanDataModel> errorLoans);
        string GetErrorLoansFileLocation(string filename);
    }

    public class LoanService : ILoanService
    {
        private readonly ITemplateRepository _templateRepository;
        private readonly IImportSessionRepository _importSessionRepository;
        private readonly IClientLoanRepository _clientLoanRepository;
        private readonly IBreezePropertyService _breezePropertyService;

        private readonly string _exportFileLocation;
        private readonly string _errorLoansFileLocation;

        public LoanService(ITemplateRepository templateRepository, IImportSessionRepository importSessionRepository,
                          IClientLoanRepository clientLoanRepository, IBreezePropertyService breezePropertyService)
        {
            _templateRepository = templateRepository;
            _importSessionRepository = importSessionRepository;
            _clientLoanRepository = clientLoanRepository;
            _breezePropertyService = breezePropertyService;
            _exportFileLocation = ConfigurationProvider.GetValue<string>(SupportedConfiguration.ExportedErrorLoansFilesLocation);
            _errorLoansFileLocation = ConfigurationProvider.GetValue<string>(SupportedConfiguration.ExportedErrorLoansFilesLocation);
        }

        public async Task<PreviewClientLoansModel> ExtractClientLoansFromPostedFile(int clientId, int templateId)
        {
            var columnsMapping = (await _templateRepository.GetById(templateId)).ColumnMappings.ToList();

            var data = ExtractData(new HttpPostedFileHandler());
            var headers = data.Item1;
            var rowsData = data.Item2;

            if (!columnsMapping.Any(x => x.LoanDataPropertyName.Equals("CorrespondentId")))
            {
                var corresKey = clientId == 1 ? "BROKER ID" : "CLIENT ID";

                columnsMapping.Add(new ColumnMapping
                {
                    BreezeColumnName = "CORRESPONDENTID",
                    ClientColumnName = corresKey,
                    LoanDataPropertyName = "CorrespondentId",
                    ClientColumnDisplayOrder = -2
                });

                rowsData.ForEach(rowData =>
                {
                    var correspondentId = string.Empty;
                    if (clientId == 1)
                    {
                        var brokerIdCell = rowData.Cells.FirstOrDefault(x => x.Name.ToLower() == "broker id");
                        if (brokerIdCell != null)
                        {
                            correspondentId = brokerIdCell.Value;
                        }
                    }
                    else
                    {
                        correspondentId = clientId.ToString();
                    }
                    rowData.Cells.Add(new CellModel
                    {
                        Name = corresKey,
                        Value = correspondentId,
                        CellReference = ""
                    });
                });
            }

            bool isCalculatingAppraisalValue = !columnsMapping.Any(x => x.LoanDataPropertyName.ToLower().Equals("appraisalvalue"));
            if (isCalculatingAppraisalValue)
            {
                columnsMapping.Add(new ColumnMapping
                {
                    BreezeColumnName = "APPRAISAL VALUE",
                    ClientColumnName = "APPRAISAL VALUE",
                    LoanDataPropertyName = "AppraisalValue",
                    ClientColumnDisplayOrder = 1000
                });

                rowsData.ForEach(rowData =>
                {
                    var loanAmountMapping = columnsMapping.FirstOrDefault(x => x.LoanDataPropertyName.ToLower().Equals("loanamount"));
                    var ltvMapping = columnsMapping.FirstOrDefault(x => x.LoanDataPropertyName.ToLower().Equals("ltv"));

                    decimal? appraisalValue = null;
                    if (loanAmountMapping != null && ltvMapping != null)
                    {
                        var loanAmountCell = rowData.Cells.FirstOrDefault(x => x.Name.ToUpper() == loanAmountMapping.ClientColumnName.ToUpper());
                        var ltvCell = rowData.Cells.FirstOrDefault(x => x.Name.ToUpper() == ltvMapping.ClientColumnName.ToUpper());
                        if (loanAmountCell != null && ltvCell != null)
                        {
                            decimal loanAmount = 0;
                            decimal.TryParse(loanAmountCell.Value, out loanAmount);
                            decimal ltvValue = 0;
                            decimal.TryParse(ltvCell.Value, out ltvValue);
                            if (ltvValue == 0)
                            {
                                appraisalValue = decimal.Floor(loanAmount);
                            }
                            else
                            {
                                appraisalValue = decimal.Floor(loanAmount / ltvValue * 100);
                            }
                        }
                    }
                    rowData.Cells.Add(new CellModel
                    {
                        Name = "APPRAISAL VALUE",
                        Value = appraisalValue.HasValue ? appraisalValue.Value.ToString() : string.Empty,
                        CellReference = ""
                    });
                });
            }

            var existingClientLoans = await GetClientLoans(clientId);

            var props = _breezePropertyService.GetBreezeProperties();

            return new PreviewClientLoansModel
            {
                Header = (from prop in props
                          let isAppraisalValue = prop.PropertyName.Equals("AppraisalValue")
                          let colMapping = columnsMapping.FirstOrDefault(c => !string.IsNullOrEmpty(c.LoanDataPropertyName) && c.LoanDataPropertyName.Equals(prop.PropertyName))
                          let notMapped = colMapping == null
                          let clientFileColumnName = colMapping?.ClientColumnName ?? string.Empty
                          let tooltip = notMapped
                                        ? "not mapped"
                                        : !isAppraisalValue
                                        ? $"mapped to {colMapping.ClientColumnName}"
                                        : isCalculatingAppraisalValue
                                        ? "= Loan Amount / LTV"
                                        : $"mapped to {colMapping.ClientColumnName}"
                          select new PreviewLoanHeaderModel()
                          {
                              BreezeColumnName = prop.DisplayName,
                              IsMandatory = prop.Mandatory,
                              ClientFileColumnName = clientFileColumnName,
                              TooltipTitle = tooltip,
                              DisplayOrder = colMapping?.ClientColumnDisplayOrder ?? 1000
                          }).ToList(),
                Data = MapFileData(rowsData, columnsMapping, existingClientLoans, props)
            };
        }

        private List<ClientLoanDataModel> MapFileData(List<RowModel> rowsData,
            List<ColumnMapping> columnsMapping,
            List<ClientLoan> existingClientLoans,
            List<BreezeProperty> props)
        {
            List<ClientLoanDataModel> clientLoans = new List<ClientLoanDataModel>();

            foreach (var rowData in rowsData.Where(x => x.Cells.Count > 0))
            {
                rowData.Cells.ForEach(cell => cell.Name.ToUpper());

                var clientLoan = new ClientLoanDataModel();
                clientLoan.LineNumber = int.Parse(rowData.LineNumber);
                clientLoan.LoanProps = new List<LoanProp>();

                foreach (var prop in props)
                {
                    var mapping = columnsMapping.FirstOrDefault(x => !string.IsNullOrEmpty(x.LoanDataPropertyName) && x.LoanDataPropertyName.ToLower().Equals(prop.PropertyName.ToLower()));

                    var isMandatory = prop.Mandatory;
                    var cell = mapping == null ? null : rowData.Cells.FirstOrDefault(x => x.Name.Equals(mapping.ClientColumnName.ToUpper()));
                    var cellReference = cell?.CellReference ?? string.Empty;
                    var displayOrder = mapping?.ClientColumnDisplayOrder ?? 1000;
                    var stringValue = (mapping == null || cell == null) ? string.Empty : cell.Value;
                    var stringHandler = prop.BreezeStringHandler(stringValue);
                    var validateDataResult = string.IsNullOrEmpty(stringValue)
                                            ? new BreezeValidationResult { Valid = true }
                                            : stringHandler.Validate();

                    clientLoan.LoanProps.Add(new LoanProp
                    {
                        MissingMapping = mapping == null,
                        StringValue = stringHandler.GetStringValue(),
                        BreezePropName = prop.PropertyName,
                        BreezeColumnName = prop.DisplayName,
                        ClientColumnName = mapping?.ClientColumnName ?? string.Empty,
                        IsMandatory = isMandatory,
                        InvalidValue = !validateDataResult.Valid,
                        ValidateDataErrorMessage = validateDataResult.ErrorMessage,
                        DisplayValue = stringHandler.GetDisplayString(),
                        CellReference = cellReference,
                        DisplayOrder = displayOrder
                    }); 
                }

                if (clientLoan.LoanProps.Any(x => !string.IsNullOrWhiteSpace(x.StringValue)))
                {
                    clientLoan.CheckDuplicated(existingClientLoans);
                    clientLoans.Add(clientLoan);
                }

            }

            return clientLoans;
        }

        public async Task<ImportSessionModel> ImportLoans(int importSessionId, int clientId, int templateId, string loansString, HttpPostedFileBase file)
        {
            var loans = JsonConvert.DeserializeObject<List<ClientLoanDataModel>>(loansString);
            if (loans.Any())
            {
                var filename = file.FileName;
                var hardFileName = $"{DateTime.Now.ToString("yyyyMMddHHmmssfff")}_{filename}";

                ImportSession importSession = null;
                if (importSessionId == 0)
                {
                    importSession = await _importSessionRepository.CreateImportSession(new ImportSession()
                    {
                        ClientId = clientId,
                        TemplateId = templateId,
                        UploadFile = filename,
                        HardFile = hardFileName
                    });

                    SaveUploadedFile(file, hardFileName);
                }
                else
                {
                    importSession = await _importSessionRepository.GetImportSessionById(importSessionId);
                }

                var importResults = new List<ImportClientLoanResult>();

                foreach (var loan in loans)
                {
                    var clientLoanNumber = loan.LoanProps.FirstOrDefault(x => x.BreezePropName.Equals("ClientLoanNumber"));
                    try
                    {
                        if (loan.IsDuplicated)
                        {
                            await _clientLoanRepository.Delete(loan.DuplicatedLoanId);
                        }

                        await _importSessionRepository.AddClientLoan(importSession, new ClientLoan()
                        {
                            LineNumber = loan.LineNumber,
                            ImportDate = DateTime.Now,
                            LoanProperties = loan.LoanProps.Select(x => new LoanProperty
                            {
                                PropertyName = x.BreezePropName,
                                Value = x.StringValue
                            }).ToList()
                        });

                        importResults.Add(new ImportClientLoanResult
                        {
                            LineNumber = loan.LineNumber,
                            ClientLoanNumber = clientLoanNumber?.StringValue ?? string.Empty,
                            IsSuccess = true,
                            IsOverridden = loan.IsDuplicated
                        });

                    }
                    catch (Exception e)
                    {
                        Log.Instance.Error(e);

                        importResults.Add(new ImportClientLoanResult
                        {
                            LineNumber = loan.LineNumber,
                            ClientLoanNumber = clientLoanNumber?.StringValue ?? string.Empty,
                            IsSuccess = false,
                            ErrorMessage = e.Message,
                            IsOverridden = loan.IsDuplicated
                        });
                    }

                }

                return new ImportSessionModel
                {
                    ImportSessionId = importSession.ImportSessionId,
                    ImportResults = importResults
                };
            }

            return null;
        }

        private void SaveUploadedFile(HttpPostedFileBase file, string saveWithName)
        {
            try
            {
                var filePath = Path.Combine(ConfigurationProvider.GetValue<string>(SupportedConfiguration.ImportFilesLocation), saveWithName);
                file.SaveAs(filePath);
            }
            catch (Exception e)
            {
                Log.Instance.Error(e.Message);
            }
        }

        private Tuple<RowModel, List<RowModel>> ExtractData(FileHandler fileHandler)
        {
            var headers = new RowModel();
            List<RowModel> rowsData = new List<RowModel>();
            fileHandler.ExtractData(ref headers, ref rowsData);

            return new Tuple<RowModel, List<RowModel>>(headers, rowsData);
        }

        private async Task<List<ClientLoan>> GetClientLoans(int clientId)
        {
            return await _clientLoanRepository.GetByClientId(clientId);
        }



        public string GetExportedDataFileLocation(string filename)
        {
            return Path.Combine(_exportFileLocation, filename);
        }

        public async Task<string> GenerateErrorLoansFile(List<ClientLoanDataModel> errorLoans)
        {
            var data = ExtractData(new HttpPostedFileHandler());
            var header = data.Item1;
            var rowsData = data.Item2;

            FileWriter writer = new ExcelWriter();

            List<Row> rows = new List<Row>();
            rows.Add(CreateHeaderRowForExcel(writer, header));
            errorLoans.ForEach(loan =>
            {
                rows.Add(GenerateRowForChildPartDetail(writer, loan, rowsData));
            });

            string fileName = $"Exported_Error_Loans_{DateTime.Now.ToString("yyyyMdHmsfff")}.xlsx";
            string exportedFile = Path.Combine(_errorLoansFileLocation, fileName);

            var result = writer.GenerateFile(exportedFile, rows);

            return result ? fileName : string.Empty;
        }

        private Row CreateHeaderRowForExcel(FileWriter writer, RowModel header)
        {
            Row workRow = new Row();
            foreach (var cell in header.Cells)
            {
                workRow.Append(writer.CreateCell(cell.Value));
            }
            return workRow;
        }

        private Row GenerateRowForChildPartDetail(FileWriter writer, ClientLoanDataModel errorLoan, List<RowModel> extractedRowsData)
        {
            Row workRow = new Row();
            var rowData = extractedRowsData.FirstOrDefault(x => x.LineNumber == errorLoan.LineNumber.ToString());
            rowData.Cells.ForEach(cell =>
            {
                var lp = errorLoan.LoanProps.FirstOrDefault(x => x.ClientColumnName.ToUpper() == cell.Name.ToUpper());
                bool isErrorCell = lp != null && (lp.MissingValue || lp.InvalidValue);
                workRow.Append(writer.CreateCell(cell.Value, isErrorCell));
            });
            return workRow;
        }

        public string GetErrorLoansFileLocation(string filename)
        {
            return Path.Combine(_errorLoansFileLocation, filename);
        }
    }
}