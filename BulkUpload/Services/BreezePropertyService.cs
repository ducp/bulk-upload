﻿using BulkUpload.Entities;
using BulkUpload.Helpers;
using BulkUpload.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BulkUpload.Services
{
    public interface IBreezePropertyService
    {
        List<BreezeProperty> GetBreezeProperties();
    }

    public interface IConfigurableBreezePropertyService : IBreezePropertyService
    {
        List<BreezeProperty> InitBreezePropertiesFromLoanDataModel();
        ActionResultModel Add(BreezeProperty breezeProperty);
        ActionResultModel Update(BreezeProperty breezeProperty);
        ActionResultModel Delete(string propertyName);
    }

    public class BreezePropertyService : IBreezePropertyService
    {
        public List<BreezeProperty> GetBreezeProperties()
        {
            return typeof(LoanData).GetProperties()
                                          .Where(x => x.HasBreezeColumnNameAttribute())
                                          .Select(x => new BreezeProperty
                                          {
                                              PropertyName = x.Name,
                                              DisplayName = x.GetBreezeColumnNameAttributeValue().ToUpper(),
                                              DataType = x.PropertyType.Name.ToUpper(),
                                              Mandatory = x.IsMandatory(),
                                              MaxStringLength = x.GetStringLength(),
                                              Order = x.GetFieldDisplayOrder(),
                                              ExportUsingName = x.GetExportHeaderNameAttributeValue()
                                          }).OrderBy(x => x.Order).ToList();
        }
    }

    public class ConfigurableBreezePropertyService : IConfigurableBreezePropertyService
    {
        private readonly string _jsonFilePath;

        public ConfigurableBreezePropertyService(string jsonFilePath)
        {
            _jsonFilePath = jsonFilePath;
        }

        public List<BreezeProperty> GetBreezeProperties()
        {
            if (!System.IO.File.Exists(_jsonFilePath))
            {
                Log.Instance.Warn("Breeze Properties file not existing.");
                return new List<BreezeProperty>();
            }

            string json = System.IO.File.ReadAllText(_jsonFilePath);
            var data = JsonConvert.DeserializeObject<BreezePropertiesModel>(json);

            return data?.Properties?.OrderBy(x => x.Order)?.ToList() ?? new List<BreezeProperty>();
        }

        public List<BreezeProperty> InitBreezePropertiesFromLoanDataModel()
        {
            var properties = typeof(LoanData).GetProperties()
                                          .Where(x => x.HasBreezeColumnNameAttribute())
                                          .Select(x => new BreezeProperty
                                          {
                                              PropertyName = x.Name,
                                              DisplayName = x.GetBreezeColumnNameAttributeValue().ToUpper(),
                                              DataType = x.PropertyType.Name.ToUpper(),
                                              Mandatory = x.IsMandatory(),
                                              MaxStringLength = x.GetStringLength(),
                                              Order = x.GetFieldDisplayOrder(),
                                              ExportUsingName = x.GetExportHeaderNameAttributeValue()
                                          }).OrderBy(x => x.DisplayName).ToList();

            var data = new BreezePropertiesModel
            {
                Properties = properties
            };

            System.IO.File.WriteAllText(_jsonFilePath, JsonConvert.SerializeObject(data));

            return data.Properties;
        }

        public ActionResultModel Add(BreezeProperty breezeProperty)
        {
            try
            {
                var properties = GetBreezeProperties();
                if (properties.Any(x => x.PropertyName.ToLower().Equals(breezeProperty.PropertyName.ToLower())))
                {
                    throw new Exception("Duplicated property name.");
                }

                breezeProperty.DisplayName = breezeProperty.DisplayName.ToUpper();

                var propertiesWithGreaterOrder = properties.Where(x => x.Order >= breezeProperty.Order).ToList();
                if (!propertiesWithGreaterOrder.Any())
                {
                    properties.Add(breezeProperty);
                }
                else
                {
                    // insert
                    var insertBeforeProperty = propertiesWithGreaterOrder.OrderBy(x => x.Order).FirstOrDefault();
                    properties.Insert(properties.IndexOf(insertBeforeProperty), breezeProperty);
                    // re-order properties
                    if (insertBeforeProperty.Order == breezeProperty.Order)
                    {
                        propertiesWithGreaterOrder.ForEach(prop => prop.Order += 1);
                    }
                }

                var data = new BreezePropertiesModel
                {
                    Properties = properties
                };

                System.IO.File.WriteAllText(_jsonFilePath, JsonConvert.SerializeObject(data));

                return new ActionResultModel
                {
                    IsSuccess = true
                };

            }
            catch (Exception e)
            {
                Log.Instance.Error(e);
                return new ActionResultModel
                {
                    IsSuccess = false,
                    ErrorMessage = e.Message
                };
            }
        }

        public ActionResultModel Delete(string propertyName)
        {
            try
            {
                var properties = GetBreezeProperties();
                var propToDelete = properties.FirstOrDefault(x => x.PropertyName.ToLower().Equals(propertyName.ToLower()));
                if (propToDelete == null)
                {
                    throw new Exception($"No property with name {propertyName}.");
                }

                // delete property
                properties.Remove(propToDelete);
                // re-order
                properties.Where(x => x.Order >= propToDelete.Order).ToList().ForEach(prop => prop.Order -= 1);

                var data = new BreezePropertiesModel
                {
                    Properties = properties
                };

                System.IO.File.WriteAllText(_jsonFilePath, JsonConvert.SerializeObject(data));

                return new ActionResultModel
                {
                    IsSuccess = true
                };

            }
            catch (Exception e)
            {
                Log.Instance.Error(e);
                return new ActionResultModel
                {
                    IsSuccess = false,
                    ErrorMessage = e.Message
                };
            }
        }

        public ActionResultModel Update(BreezeProperty breezeProperty)
        {
            try
            {
                var properties = GetBreezeProperties();
                var propToUpdate = properties.FirstOrDefault(x => x.PropertyName.ToLower().Equals(breezeProperty.PropertyName.ToLower()));
                if (propToUpdate == null)
                {
                    throw new Exception($"No property with name {breezeProperty.PropertyName}.");
                }

                propToUpdate.DisplayName = breezeProperty.DisplayName.ToUpper();
                propToUpdate.DataType = breezeProperty.DataType;
                propToUpdate.Mandatory = breezeProperty.Mandatory;
                propToUpdate.MaxStringLength = breezeProperty.MaxStringLength;
                propToUpdate.Options = breezeProperty.Options;
                propToUpdate.RegrexPattern = breezeProperty.RegrexPattern;
                propToUpdate.ExportUsingName = breezeProperty.ExportUsingName;
                propToUpdate.IsCurrency = breezeProperty.IsCurrency;
                propToUpdate.RoundNumberDown = breezeProperty.RoundNumberDown;

                var oldOrder = propToUpdate.Order;
                var newOrder = breezeProperty.Order;
                if (oldOrder != newOrder)
                {
                    if (newOrder < oldOrder)
                    {
                        properties.Where(x => x.Order >= newOrder && x.Order < oldOrder).ToList().ForEach(prop => prop.Order += 1);
                    }
                    else if (newOrder > oldOrder)
                    {
                        properties.Where(x => x.Order > oldOrder && x.Order <= newOrder).ToList().ForEach(prop => prop.Order -= 1);
                    }
                }

                propToUpdate.Order = breezeProperty.Order;

                var data = new BreezePropertiesModel
                {
                    Properties = properties
                };

                System.IO.File.WriteAllText(_jsonFilePath, JsonConvert.SerializeObject(data));

                return new ActionResultModel
                {
                    IsSuccess = true
                };

            }
            catch (Exception e)
            {
                Log.Instance.Error(e);
                return new ActionResultModel
                {
                    IsSuccess = false,
                    ErrorMessage = e.Message
                };
            }
        }
    }
}