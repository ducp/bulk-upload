﻿using BulkUpload.Entities;
using BulkUpload.Helpers;
using BulkUpload.Models;
using BulkUpload.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BulkUpload.Services
{
    public interface IColumnMappingService
    {
        Task<ColumnMapping> GetById(int id);
        Task<List<ColumnMapping>> GetColumnMappingsByTemplate(int templateId);
        Task<ColumnMapping> Insert(ColumnMapping columnMapping);
        List<CellModel> GetHeaderColumns(HttpPostedFileBase file);
        List<SelectListItemModel> GetOptionsBreezeColumns();
    }

    public class ColumnMappingService : IColumnMappingService
    {
        private readonly IColumnMappingRepository _columnMappingRepository;
        private readonly ITemplateRepository _templateRepository;
        private readonly IBreezePropertyService _breezePropertyService;

        public ColumnMappingService(IColumnMappingRepository columnMappingRepository,
                                    ITemplateRepository templateRepository,
                                    IBreezePropertyService breezePropertyService)
        {
            _columnMappingRepository = columnMappingRepository;
            _templateRepository = templateRepository;
            _breezePropertyService = breezePropertyService;
        }

        public async Task<ColumnMapping> Insert(ColumnMapping columnMapping)
        {
            return await _columnMappingRepository.Insert(columnMapping);
        }


        public async Task<List<ColumnMapping>> GetColumnMappingsByTemplate(int templateId)
        {
            return await _columnMappingRepository.GetColumnMappingsByTemplate(templateId);
        }

        public List<CellModel> GetHeaderColumns(HttpPostedFileBase file)
        {
            var fileHandler = new HttpPostedFileHandler();

            RowModel headers = new RowModel();
            List<RowModel> data = new List<RowModel>();

            fileHandler.ExtractData(ref headers, ref data, onlyHeader: true);

            return headers.Cells.OrderBy(x => x.Name).ToList();
        }

        public async Task<ColumnMapping> GetById(int id)
        {
            return await _columnMappingRepository.GetById(id);
        }

        public List<SelectListItemModel> GetOptionsBreezeColumns()
        {
            var props = _breezePropertyService.GetBreezeProperties();
            return props.Select(x => new SelectListItemModel
            {
                Text = x.DisplayName,
                Value = x.PropertyName,
                Mandatory = x.Mandatory
            }).OrderBy(x => x.Text).ToList();
        }
    }
}