﻿using BulkUpload.Entities;
using BulkUpload.Helpers;
using BulkUpload.Models;
using BulkUpload.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BulkUpload.Services
{
    public interface ITemplateService
    {
        Task Delete(int id);
        Task<List<Template>> GetAllTemplates();
        Task<Template> GetById(int? id);
        Task<Template> Insert(Template template);
        Task CloneTemplate(int id, string name, int clientid);
        Task<List<Template>> GetByClientId(int clientId);
        Task<Template> GetTemplateByName(int clientId, string newTemplateName);
        Task<ActionResultModel> CreateTemplate(string templateName, int clientId, string columnsMappingString, HttpFileCollectionBase files);
        Task<ActionResultModel> UpdateTemplate(int templateId, string templateName, int clientId, string columnMappings, HttpFileCollectionBase files);
        Task<TemplateDetailModel> GetTemplateInfoById(int templateId);
    }

    public class TemplateService : ITemplateService
    {
        private readonly ITemplateRepository _templateRepository;
        private readonly IColumnMappingRepository _columnMappingRepository;

        public TemplateService(ITemplateRepository templateRepository, IColumnMappingRepository columnMappingRepository)
        {
            _templateRepository = templateRepository;
            _columnMappingRepository = columnMappingRepository;
        }

        public async Task<Template> Insert(Template template)
        {
            return await _templateRepository.Insert(template); ;
        }

        public async Task<Template> GetById(int? id)
        {
            return await _templateRepository.GetById(id); ;
        }

        public async Task Delete(int id)
        {
            await _templateRepository.Delete(id);
        }

        public async Task<List<Template>> GetAllTemplates()
        {
            return await _templateRepository.GetAllTemplates();
        }

        public async Task CloneTemplate(int id, string name, int clientid)
        {
            var template = await _templateRepository.GetById(id);
            if (template != null)
            {
                // copy hard file
                var copiedFileInfo = CopyTemplateHardFile(template);

                Template tem = new Template();
                tem.TemplateName = name;
                tem.ClientId = clientid;
                tem.UploadFile = copiedFileInfo.Item1;
                tem.HardFile = copiedFileInfo.Item2;
                tem.IsDelete = false;
                tem.ColumnMappings = template.ColumnMappings.Select(x => new ColumnMapping
                {
                    ClientColumnName = x.ClientColumnName,
                    BreezeColumnName = x.BreezeColumnName,
                    LoanDataPropertyName = x.LoanDataPropertyName
                }).ToList();
                tem.CreatedDate = DateTime.Now;

                await _templateRepository.Insert(tem);
            }
        }

        private Tuple<string, string> CopyTemplateHardFile(Template fromTemplate)
        {
            try
            {
                if (string.IsNullOrEmpty(fromTemplate.UploadFile) || string.IsNullOrEmpty(fromTemplate.HardFile))
                {
                    throw new Exception($"template file empty for template: {fromTemplate.TemplateId}");
                }

                var source = Path.Combine(ConfigurationProvider.GetValue<string>(SupportedConfiguration.TemplatesLocation), fromTemplate.HardFile);
                var newFile = $"{DateTime.Now.ToString("yyyyMMddHHmmssfff")}_{fromTemplate.UploadFile}";
                var target = Path.Combine(ConfigurationProvider.GetValue<string>(SupportedConfiguration.TemplatesLocation), newFile);

                File.Copy(source, target);

                return new Tuple<string, string>(fromTemplate.UploadFile, newFile);
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex);
                return new Tuple<string, string>(string.Empty, string.Empty);
            }
        }

        public async Task<List<Template>> GetByClientId(int clientId)
        {
            return await _templateRepository.GetByClientId(clientId);
        }

        public async Task<Template> GetTemplateByName(int clientId, string newTemplateName)
        {
            return await _templateRepository.GetTemplateByName(clientId, newTemplateName);
        }

        public async Task<ActionResultModel> CreateTemplate(string templateName, int clientId, string columnsMappingString, HttpFileCollectionBase files)
        {
            if (await _templateRepository.GetTemplateByName(clientId, templateName) != null)
            {
                return new ActionResultModel { IsSuccess = false, ErrorMessage = $"The name {templateName} is already in use" };
            }

            var fileReturn = SaveFileUpload(files);
            var columnsMapping = JsonConvert.DeserializeObject<List<ColumnMapping>>(columnsMappingString);
            var template = new Template
            {
                ClientId = clientId,
                TemplateName = templateName,
                UploadFile = fileReturn.Item1,
                HardFile = fileReturn.Item2,
                CreatedDate = DateTime.Now,
                ColumnMappings = columnsMapping.Select(x => new ColumnMapping
                {
                    ClientColumnName = x.ClientColumnName,
                    BreezeColumnName = x.BreezeColumnName,
                    LoanDataPropertyName = x.LoanDataPropertyName,
                    ClientColumnDisplayOrder = x.ClientColumnDisplayOrder
                }).ToList()
            };
            await _templateRepository.Insert(template);
            return new ActionResultModel { IsSuccess = true };
        }

        public async Task<ActionResultModel> UpdateTemplate(int templateId, string templateName, int clientId, string columnMappings, HttpFileCollectionBase files)
        {
            var template = await _templateRepository.GetTemplateByName(clientId, templateName);
            if (template != null && template.TemplateId != templateId)
            {
                return new ActionResultModel { IsSuccess = false, ErrorMessage = $"The name {templateName} is already in use" };
            }

            var fileReturn = new Tuple<string, string>(string.Empty, string.Empty);
            if (files.Count > 0)
            {
                template = await _templateRepository.GetById(templateId);
                var pathToRemove = Path.Combine(ConfigurationProvider.GetValue<string>(SupportedConfiguration.TemplatesLocation), template.HardFile);
                if (File.Exists(pathToRemove))
                {
                    File.Delete(pathToRemove);
                }
                fileReturn = SaveFileUpload(files);
            }

            var mappings = JsonConvert.DeserializeObject<List<ColumnMapping>>(columnMappings);
            await _templateRepository.Update(templateId, templateName, clientId, mappings, fileReturn.Item1, fileReturn.Item2);
            return new ActionResultModel { IsSuccess = true };
        }

        private Tuple<string, string> SaveFileUpload(HttpFileCollectionBase files)
        {
            try
            {
                HttpPostedFileBase file = files[0];
                var uploadFile = Path.GetFileName(file.FileName);
                var hardFile = $"{DateTime.Now.ToString("yyyyMMddHHmmssfff")}_{uploadFile}";
                var hardFilePath = Path.Combine(ConfigurationProvider.GetValue<string>(SupportedConfiguration.TemplatesLocation), hardFile);
                file.SaveAs(hardFilePath);
                return new Tuple<string, string>(uploadFile, hardFile);
            }
            catch (Exception e)
            {
                Log.Instance.Error(e);
                return new Tuple<string, string>(string.Empty, string.Empty);
            }
        }

        private List<CellModel> GetHeaderFromFile(FileHandler fileHandler)
        {
            var headers = new RowModel();
            List<RowModel> data = new List<RowModel>();
            fileHandler.ExtractData(ref headers, ref data, onlyHeader: true);
            return headers.Cells;
        }

        public async Task<TemplateDetailModel> GetTemplateInfoById(int templateId)
        {
            var template = await _templateRepository.GetById(templateId);
            if (template == null)
            {
                throw new Exception($"The template wasn't found.");
            }

            var templatesFolder = ConfigurationProvider.GetValue<string>(SupportedConfiguration.TemplatesLocation);
            var filePath = Path.Combine(templatesFolder, template.HardFile);
            var headers = GetHeaderFromFile(new SystemFileHandler(filePath));

            return new TemplateDetailModel()
            {
                TemplateId = template.TemplateId,
                TemplateName = template.TemplateName,
                ClientId = template.ClientId,
                FileName = template.UploadFile,
                ColumnMappings = template.ColumnMappings.Select(x => new ColumnMapping
                {
                    ColumnMappingId = x.ColumnMappingId,
                    BreezeColumnName = x.BreezeColumnName,
                    ClientColumnName = x.ClientColumnName,
                    LoanDataPropertyName = x.LoanDataPropertyName
                }).ToList(),
                ClientColumnHeaders = headers.OrderBy(x => x.Name).ToList()
            };
        }
    }
}
