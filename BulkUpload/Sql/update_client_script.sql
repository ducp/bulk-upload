USE [BulkDb]
GO
/****** Object:  Table [dbo].[Client]    Script Date: 3/27/2020 1:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Client](
	[ClientId] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED 
(
	[ClientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (169023, N'ML MORTGAGE CORP.')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (187561, N'SOUTHWEST FUNDING LP')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (192292, N'PACIFIC TRUST MORTGAGE')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (203688, N'JMJ FINANCIAL')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (204483, N'LAKE PACOR HOME MORTGAGE')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (207857, N'FIRST WORLD MORTGAGE CORPORATION')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (207931, N'LYNX MORTGAGE BANK LLC')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (208156, N'RESIDENTIAL WHOLESALE MORTGAGE, INC.')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (208346, N'JFK FINANCIAL, INC.')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (208920, N'ALCOVA MORTGAGE LLC')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (209349, N'CHOICE LENDING CORP.')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (209422, N'BAY CAPITAL MORTGAGE CORPORATION')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (209584, N'NATIONS RELIABLE LENDING, LLC')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (209671, N'PRIME SOURCE MORTGAGE, INC.')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (209674, N'CITYWIDE HOME LOANS, LLC')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (210684, N'ALAMEDA MORTGAGE CORPORATION')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (211497, N'FIRST OHIO HOME FINANCE, INC')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (212642, N'CRM LENDING')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (220831, N'BBMC MORTGAGE')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (221383, N'VISION ONE MORTGAGE, INC.')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (221388, N'INTERNATIONAL CITY MORTGAGE INC.')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (221762, N'COMPASS MORTGAGE LENDING, INC.')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (221814, N'ABC BANK')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (221935, N'IDAHO FIRST BANK')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (221936, N'MAGNOLIA BANK, INC.')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (221975, N'LENDING ONE SOLUTIONS')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (222034, N'PINNACLE FEDERAL CREDIT UNION')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (222274, N'PRIMARY PARTNERS FINANCIAL')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (222292, N'VICTORIAN FINANCE, LLC')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (222546, N'AIR ACADEMY FEDERAL CREDIT UNION')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (222662, N'DIRECT MORTGAGE LOANS, LLC')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (222722, N'PEOPLES MORTGAGE COMPANY')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (222740, N'ENDEAVOR CAPITAL, LLC')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (222769, N'AMERICAN FIRST LENDING CORPORATION')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (222792, N'INTERLINC MORTGAGE SERVICES, LLC')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (222951, N'PLATINUM HOME MORTGAGE CORPORATION')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (222952, N'WARSAW FEDERAL SAVINGS')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (223160, N'UNITED FIDELITY FUNDING CORP.')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (223169, N'CITIZENS NATIONAL BANK OF GREATER ST. LOUIS')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (223173, N'FIRST INTEGRITY MORTGAGE SERVICES')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (223245, N'CHICAGO FINANCIAL SERVICES, INC.')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (223545, N'PACIFIC FUNDING MORTGAGE DIVISION')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (223579, N'JET DIRECT FUNDING CORP.')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (223709, N'PARKSIDE LENDING, LLC')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (224071, N'SUMMIT MORTGAGE CORPORATION')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (224247, N'HOLLAND MORTGAGE ADVISORS')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (224411, N'MORTGAGE WORLD BANKERS, INC')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (224478, N'INFINITY MORTGAGE HOLDINGS, LLC')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (224570, N'BISON STATE BANK')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (224600, N'TRADITION MORTGAGE, LLC - COR')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (224648, N'MISSION MORTGAGE OF TEXAS, INC.')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (224754, N'SAN DIEGO FUNDING')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (224777, N'FIRST SAVINGS BANK')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (224870, N'BANC ONE MORTGAGE CORPORATION')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (224918, N'GHI MORTGAGE')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (224979, N'DIRECT LENDERS, LLC')
INSERT [dbo].[Client] ([ClientId], [Name]) VALUES (225007, N'NORTHERN MORTGAGE SERVICES, LLC')
