USE [master]
GO
/****** Object:  Database [BulkUpload]    Script Date: 11/1/2019 3:19:10 PM ******/
CREATE DATABASE [BulkUpload]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BulkUpload', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\BulkUpload.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'BulkUpload_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\BulkUpload_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [BulkUpload] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BulkUpload].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BulkUpload] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BulkUpload] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BulkUpload] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BulkUpload] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BulkUpload] SET ARITHABORT OFF 
GO
ALTER DATABASE [BulkUpload] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BulkUpload] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BulkUpload] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BulkUpload] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BulkUpload] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BulkUpload] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BulkUpload] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BulkUpload] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BulkUpload] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BulkUpload] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BulkUpload] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BulkUpload] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BulkUpload] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BulkUpload] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BulkUpload] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BulkUpload] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BulkUpload] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BulkUpload] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [BulkUpload] SET  MULTI_USER 
GO
ALTER DATABASE [BulkUpload] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BulkUpload] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BulkUpload] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BulkUpload] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [BulkUpload] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'BulkUpload', N'ON'
GO
ALTER DATABASE [BulkUpload] SET QUERY_STORE = OFF
GO
USE [BulkUpload]
GO
/****** Object:  Table [dbo].[Client]    Script Date: 11/1/2019 3:19:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Client](
	[ClientId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED 
(
	[ClientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ClientLoan]    Script Date: 11/1/2019 3:19:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClientLoan](
	[ClientLoanId] [int] IDENTITY(1,1) NOT NULL,
	[ImportSessionId] [int] NOT NULL,
	[ImportDate] [datetime] NOT NULL,
	[LineNumber] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ClientLoanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ColumnMapping]    Script Date: 11/1/2019 3:19:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ColumnMapping](
	[ColumnMappingId] [int] IDENTITY(1,1) NOT NULL,
	[ClientColumnName] [varchar](200) NOT NULL,
	[BreezeColumnName] [varchar](200) NOT NULL,
	[TemplateId] [int] NOT NULL,
	[LoanDataPropertyName] [varchar](50) NULL,
 CONSTRAINT [PK_ColumnMapping] PRIMARY KEY CLUSTERED 
(
	[ColumnMappingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ImportSession]    Script Date: 11/1/2019 3:19:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ImportSession](
	[ImportSessionId] [int] IDENTITY(1,1) NOT NULL,
	[ClientId] [int] NOT NULL,
	[TemplateId] [int] NOT NULL,
	[UploadFile] [nvarchar](200) NOT NULL,
	[HardFile] [nvarchar](200) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ImportSessionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Loan]    Script Date: 11/1/2019 3:19:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Loan](
	[LoanId] [int] IDENTITY(1,1) NOT NULL,
	[CorrespondentId] [int] NULL,
	[CorrespondentName] [varchar](50) NULL,
	[ClientLoanNumber] [varchar](15) NULL,
	[CommitmentNumber] [varchar](15) NULL,
	[LockDate] [datetime] NULL,
	[LockTerm] [decimal](3, 0) NULL,
	[Price] [decimal](11, 2) NULL,
	[Program] [varchar](20) NULL,
	[NoteRate] [decimal](5, 3) NULL,
	[LoanAmount] [decimal](11, 2) NULL,
	[BorrowerLastName] [varchar](20) NULL,
	[PropertyType] [char](1) NULL,
	[DocType] [char](1) NULL,
	[LTV] [decimal](6, 3) NULL,
	[CLTV] [decimal](6, 3) NULL,
	[OwnerOccupied] [nchar](10) NULL,
	[Purpose] [char](1) NULL,
	[CashOut] [decimal](11, 2) NULL,
	[FICO] [decimal](3, 0) NULL,
	[ULI] [varchar](50) NULL,
	[WarehouseBank] [char](3) NULL,
	[AppraisalValue] [decimal](11, 2) NULL,
	[PurchasePrice] [decimal](11, 2) NULL,
	[Address] [nchar](10) NULL,
	[City] [varchar](max) NULL,
	[State] [varchar](max) NULL,
	[Zip] [varchar](15) NULL,
 CONSTRAINT [PK_Loan] PRIMARY KEY CLUSTERED 
(
	[LoanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LoanData]    Script Date: 11/1/2019 3:19:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoanData](
	[LoanDataId] [int] IDENTITY(1,1) NOT NULL,
	[CorrespondentId] [nvarchar](50) NOT NULL,
	[CorrespondentName] [nvarchar](50) NULL,
	[ClientLoanNumber] [nvarchar](50) NOT NULL,
	[CommitmentNumber] [nvarchar](50) NOT NULL,
	[LockDate] [datetime] NOT NULL,
	[LockTerm] [decimal](11, 2) NOT NULL,
	[Price] [decimal](11, 2) NOT NULL,
	[Program] [nvarchar](50) NOT NULL,
	[NoteRate] [decimal](11, 2) NOT NULL,
	[LoanAmount] [decimal](11, 2) NOT NULL,
	[BorrowerLastName] [nvarchar](50) NOT NULL,
	[PropertyType] [nvarchar](50) NULL,
	[DocType] [nvarchar](50) NOT NULL,
	[LTV] [decimal](11, 2) NOT NULL,
	[CLTV] [decimal](11, 2) NOT NULL,
	[OwnerOccupied] [nvarchar](50) NULL,
	[Purpose] [nvarchar](50) NULL,
	[CashOut] [decimal](11, 2) NULL,
	[FICO] [decimal](11, 2) NULL,
	[ULI] [nvarchar](50) NULL,
	[WarehouseBank] [nvarchar](50) NULL,
	[AppraisalValue] [decimal](11, 2) NULL,
	[PurchasePrice] [decimal](11, 2) NULL,
	[Address] [nvarchar](200) NOT NULL,
	[City] [nvarchar](200) NOT NULL,
	[State] [nvarchar](20) NOT NULL,
	[Zip] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK__LoanData__3214EC071A4AE1EB] PRIMARY KEY CLUSTERED 
(
	[LoanDataId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LoanProperty]    Script Date: 11/1/2019 3:19:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoanProperty](
	[LoanPropertyId] [int] IDENTITY(1,1) NOT NULL,
	[ClientLoanId] [int] NOT NULL,
	[PropertyName] [nvarchar](50) NOT NULL,
	[Value] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[LoanPropertyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RegisteredLoan]    Script Date: 11/1/2019 3:19:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RegisteredLoan](
	[ClientLoanId] [int] NOT NULL,
	[BreezeLoanId] [varchar](50) NULL,
	[RegisteredDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ClientLoanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Template]    Script Date: 11/1/2019 3:19:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Template](
	[TemplateId] [int] IDENTITY(1,1) NOT NULL,
	[ClientId] [int] NOT NULL,
	[TemplateName] [varchar](200) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UploadFile] [varchar](200) NULL,
	[HardFile] [varchar](200) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Teamplate] PRIMARY KEY CLUSTERED 
(
	[TemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ImportSession]  WITH CHECK ADD  CONSTRAINT [FK__ImportSes__Templ__46E78A0C] FOREIGN KEY([TemplateId])
REFERENCES [dbo].[Template] ([TemplateId])
GO
ALTER TABLE [dbo].[ImportSession] CHECK CONSTRAINT [FK__ImportSes__Templ__46E78A0C]
GO
ALTER TABLE [dbo].[RegisteredLoan]  WITH CHECK ADD FOREIGN KEY([ClientLoanId])
REFERENCES [dbo].[ClientLoan] ([ClientLoanId])
GO
USE [master]
GO
ALTER DATABASE [BulkUpload] SET  READ_WRITE 
GO
