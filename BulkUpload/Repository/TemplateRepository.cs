﻿using BulkUpload.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace BulkUpload.Repository
{
    public interface ITemplateRepository
    {
        Task Delete(int id);
        Task<List<Template>> GetAllTemplates();
        Task<Template> GetById(int? id);
        Task<Template> Insert(Template template);
        Task<Template> Update(int templateId, string templateName, int clientId, List<ColumnMapping> mappings, string uploadFile, string hardFile);
        Task<List<Template>> GetByClientId(int clientId);
        Task<Template> GetTemplateByName(int clientId, string newTemplateName);
    }

    public class TemplateRepository : ITemplateRepository
    {
        private readonly DbContext _dbContext;
        private readonly DbSet<Template> _entities;
        private readonly DbSet<ColumnMapping> _columnMappings;

        public TemplateRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
            _entities = _dbContext.Set<Template>();
            _columnMappings = _dbContext.Set<ColumnMapping>();
        }

        public async Task<Template> Insert(Template template)
        {
            _entities.Add(template);
            await _dbContext.SaveChangesAsync();
            return template;
        }

        public async Task<Template> GetById(int? id)
        {
            var template = await _entities.Where(x => x.TemplateId == id && !x.IsDelete).FirstOrDefaultAsync();
            return template;
        }

        public async Task<Template> Update(int templateId, string templateName, int clientId, List<ColumnMapping> mappings, string uploadFile, string hardFile)
        {
            var template = await _entities.FindAsync(templateId);
            if (template != null)
            {
                template.TemplateName = templateName;
                template.ClientId = clientId;
                if (!string.IsNullOrWhiteSpace(uploadFile))
                {
                    template.UploadFile = uploadFile;
                }
                if (!string.IsNullOrWhiteSpace(hardFile))
                {
                    template.HardFile = hardFile;
                }
                template.ColumnMappings.ToList().ForEach(o =>
                {
                    var cm = _columnMappings.Find(o.ColumnMappingId);
                    _columnMappings.Remove(cm);
                });
                mappings.ForEach(o =>
                {
                    template.ColumnMappings.Add(o);
                });
                await _dbContext.SaveChangesAsync();
                return template;
            }

            throw new System.Exception($"No template with Id: {templateId}");
        }

        public async Task Delete(int id)
        {
            var template = await _entities.FirstOrDefaultAsync(x => x.TemplateId == id);
            if (template != null)
            {
                template.IsDelete = true;
                await _dbContext.SaveChangesAsync();
            }
        }

        public async Task<List<Template>> GetAllTemplates()
        {
            return await _entities.Where(x => !x.IsDelete).OrderBy(x => x.TemplateName).ToListAsync();
        }

        public async Task<List<Template>> GetByClientId(int clientId)
        {
            return await _entities.Where(x => !x.IsDelete && x.ClientId == clientId).OrderBy(x => x.TemplateName).ToListAsync();
        }

        public async Task<Template> GetTemplateByName(int clientId, string newTemplateName)
        {
            return await _entities.FirstOrDefaultAsync(x => !x.IsDelete
                                                                        && x.ClientId == clientId
                                                                        && x.TemplateName.ToLower().Equals(newTemplateName.ToLower()));
        }
    }
}