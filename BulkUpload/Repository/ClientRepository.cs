﻿using BulkUpload.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BulkUpload.Repository
{
    public interface IClientRepository
    {
        Task<List<Client>> GetAllClients();
        Task<Client> GetById(int id);
    }
    public class ClientRepository : IClientRepository
    {
        private readonly DbContext _dbContext;
        private DbSet<Client> _entities;

        public ClientRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
            _entities = _dbContext.Set<Client>();
        }

        public async Task<Client> GetById(int id)
        {
            var client = await _entities.FindAsync(id);
            return client;
        }

        public async Task<List<Client>> GetAllClients()
        {
            var template = await _entities.OrderBy(x => x.ClientId).ToListAsync();
            return template;
        }

    }
}