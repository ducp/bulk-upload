﻿using BulkUpload.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BulkUpload.Repository
{
    public interface IColumnMappingRepository
    {
        Task Delete(int id);
        Task<ColumnMapping> GetById(int id);
        Task<ColumnMapping> Insert(ColumnMapping entity);
        Task<ColumnMapping> Update(ColumnMapping entity);
        Task<List<ColumnMapping>> GetColumnMappingsByTemplate(int templateId);
    }

    public class ColumnMappingRepository : IColumnMappingRepository
    {

        private readonly DbContext _dbContext;
        private readonly DbSet<ColumnMapping> _entities;

        public ColumnMappingRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
            _entities = _dbContext.Set<ColumnMapping>();
        }

        public async Task<ColumnMapping> Insert(ColumnMapping entity)
        {
            _entities.Add(entity);
            await _dbContext.SaveChangesAsync();
            return entity;
        }

        public async Task<ColumnMapping> Update(ColumnMapping entity)
        {
            _dbContext.Entry(entity).CurrentValues.SetValues(entity);
            await _dbContext.SaveChangesAsync();

            return entity;
        }

        public async Task Delete(int id)
        {
            var entity = _entities.Where(x => x.ColumnMappingId == id).FirstOrDefault();
            _entities.Remove(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<ColumnMapping> GetById(int id)
        {
            return await _entities.Where(x => x.ColumnMappingId == id).FirstOrDefaultAsync();
        }

        public async Task<List<ColumnMapping>> GetColumnMappingsByTemplate(int templateId)
        {
            return await _entities.Where(x => x.TemplateId == templateId).ToListAsync();
        }
    }
}