﻿using BulkUpload.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BulkUpload.Repository
{
    public interface IRegisteredLoanRepository
    {
        Task<List<RegisteredLoan>> GetAllRegisteredLoan(List<int> clientIds);
        Task<RegisteredLoan> Insert(RegisteredLoan model);
        Task<RegisteredLoan> GetByClientLoanId(int id);
    }
    public class RegisteredLoanRepository : IRegisteredLoanRepository
    {
        private readonly DbContext _dbContext;
        private readonly DbSet<RegisteredLoan> _entities;

        public RegisteredLoanRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
            _entities = _dbContext.Set<RegisteredLoan>();
        }

        public async Task<List<RegisteredLoan>> GetAllRegisteredLoan(List<int> clientIds)
        {
            return await _entities.Where(x => clientIds.Contains(x.ClientLoan.ImportSession.ClientId)).ToListAsync();
        }

        public async Task<RegisteredLoan> Insert(RegisteredLoan model)
        {
            _entities.Add(model);
            await _dbContext.SaveChangesAsync();
            return model;
        }

        public async Task<RegisteredLoan> GetByClientLoanId(int clientLoanId)
        {
            return await _entities.FindAsync(clientLoanId);
        }

    }
}