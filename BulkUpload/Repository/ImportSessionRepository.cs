﻿using BulkUpload.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BulkUpload.Repository
{
    public interface IImportSessionRepository
    {
        Task<ImportSession> CreateImportSession(ImportSession importSession);
        Task<ImportSession> GetImportSessionById(int id);
        Task<ImportSession> AddClientLoans(int importSessionId, List<ClientLoan> clientLoans);
        Task AddClientLoan(ImportSession importSession, ClientLoan clientLoan);
        Task<List<ImportSession>> GetImportSessionsByClient(int clientId);
    }

    public class ImportSessionRepository : IImportSessionRepository
    {
        private readonly DbContext _dbContext;
        private readonly DbSet<ImportSession> _entities;

        public ImportSessionRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
            _entities = _dbContext.Set<ImportSession>();
        }

        public async Task<ImportSession> GetImportSessionById(int id)
        {
            return await _entities.FindAsync(id);
        }

        public async Task<ImportSession> CreateImportSession(ImportSession importSession)
        {
            _entities.Add(importSession);
            await _dbContext.SaveChangesAsync();
            return importSession;
        }

        public async Task<ImportSession> AddClientLoans(int importSessionId, List<ClientLoan> clientLoans)
        {
            var importSession = await GetImportSessionById(importSessionId);
            if (importSession != null)
            {
                var clientLoanList = importSession.ClientLoans.Union(clientLoans).ToList();
                importSession.ClientLoans = clientLoanList;
                await _dbContext.SaveChangesAsync();

                return importSession;
            }

            return null;
        }

        public async Task AddClientLoan(ImportSession importSession, ClientLoan clientLoan)
        {
            importSession.ClientLoans.Add(clientLoan);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<ImportSession>> GetImportSessionsByClient(int clientId)
        {
            return await _entities.Where(x => x.ClientId == clientId).ToListAsync();
        }
    }
}