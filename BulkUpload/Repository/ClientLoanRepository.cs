﻿using BulkUpload.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BulkUpload.Repository
{
    public interface IClientLoanRepository
    {
        Task<List<ClientLoan>> GetAllLoanData(List<int> clientIds, string keyword);
        Task<ClientLoan> GetById(int id);
        Task Delete(int id);
        Task<List<ClientLoan>> GetByClientId(int clientId);
        Task<ClientLoan> AddBreezeLoan(int clientLoanId, RegisteredLoan registeredLoan);
        Task<List<ClientLoan>> GetByIds(List<int> clientLoanIds);
    }

    public class ClientLoanRepository : IClientLoanRepository
    {
        private readonly DbContext _dbContext;
        private readonly DbSet<ClientLoan> _entities;

        public ClientLoanRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
            _entities = _dbContext.Set<ClientLoan>();
        }

        public async Task<List<ClientLoan>> GetAllLoanData(List<int> clientIds, string keyword)
        {
            return await _entities.Where(x => x.IsDeleted != true)
                                  .Where(x => x.RegisteredLoan == null)
                                  .Where(x => clientIds.Contains(x.ImportSession.ClientId))
                                  .Where(x => string.IsNullOrEmpty(keyword) || x.LoanProperties.Any(lp => lp.Value.ToLower().Contains(keyword.ToLower())))
                                  .OrderByDescending(x => x.ImportDate)
                                  .ToListAsync();
        }

        public async Task Delete(int id)
        {
            var clientLoan = _entities.Find(id);
            if (clientLoan != null)
            {
                clientLoan.IsDeleted = true;
                await _dbContext.SaveChangesAsync();
            }
        }

        public async Task<ClientLoan> GetById(int id)
        {
            return await _entities.FindAsync(id);
        }

        public async Task<List<ClientLoan>> GetByClientId(int clientId)
        {
            return await _entities.Where(x => !x.IsDeleted && x.ImportSession.ClientId == clientId).ToListAsync();
        }

        public async Task<ClientLoan> AddBreezeLoan(int clientLoanId, RegisteredLoan registeredLoan)
        {
            var clientLoan = _entities.Find(clientLoanId);
            clientLoan.RegisteredLoan = registeredLoan;
            await _dbContext.SaveChangesAsync();
            return clientLoan;
        }

        public async Task<List<ClientLoan>> GetByIds(List<int> clientLoanIds)
        {
            return await _entities.Where(x => clientLoanIds.Contains(x.ClientLoanId)).ToListAsync();
        }
    }
}