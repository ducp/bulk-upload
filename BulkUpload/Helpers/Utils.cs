﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BulkUpload.Helpers
{
    public static class Utils
    {
        public static DateTime ConvertFromDateString(string stringValue)
        {
            try
            {
                return DateTime.Parse(stringValue);
            }
            catch (Exception e)
            {
                Log.Instance.Error(e.Message);
                return DateTime.MinValue;
            }
        }

        public static List<string> GetEnumValues<T>() where T : Enum
        {
            return Enum.GetValues(typeof(T)).Cast<T>().Select(x => x.ToString()).ToList();
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            Random random = new Random(SeedProvider.GetSeed());
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

    }


    public class SeedProvider
    {
        private static int _seed;

        public static int GetSeed()
        {
            if (_seed == 0)
            {
                _seed = (int)DateTime.Now.ToOADate();
            }
            else
            {
                _seed += 1;
            }
            Log.Instance.Info($"_seed: {_seed}");
            return _seed;
        }
    }

}