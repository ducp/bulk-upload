﻿using BulkUpload.Models;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace BulkUpload.Helpers
{
    public abstract class FileReader
    {
        public abstract bool ReadFileStream(Stream fileStream, ref RowModel header, ref List<RowModel> rowsData, bool onlyHeader = false);
    }

    public class ExcelReader : FileReader
    {
        public ExcelReader() { }

        public override bool ReadFileStream(Stream fileStream, ref RowModel header, ref List<RowModel> rowsData, bool onlyHeader = false)
        {
            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(fileStream, false))
            {
                WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;
                var workbook = workbookPart.Workbook;
                var sheet = workbook.Descendants<Sheet>().First();
                WorksheetPart worksheetPart = (WorksheetPart)workbookPart.GetPartById(sheet.Id);
                SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();

                bool foundHeaderRow = false;
                foreach (Row r in sheetData.Elements<Row>())
                {
                    try
                    {
                        if (!foundHeaderRow)
                        {
                            header.LineNumber = r.RowIndex.ToString();
                            header.Cells = new List<CellModel>();
                            var cells = r.Elements<Cell>().ToList();
                            foreach (Cell cell in cells)
                            {
                                string value = GetCellValue(spreadsheetDocument, cell);
                                if (!string.IsNullOrWhiteSpace(value))
                                {
                                    foundHeaderRow = true;
                                    header.Cells.Add(new CellModel
                                    {
                                        Name = value.ToUpper(),
                                        Value = value,
                                        CellReference = cell.CellReference,
                                        DisplayOrder = cells.IndexOf(cell)
                                    });
                                }
                            }
                            if (foundHeaderRow && onlyHeader)
                            {
                                return true;
                            }
                        }
                        else
                        {
                            var rowData = new RowModel();
                            rowData.LineNumber = r.RowIndex.ToString();
                            rowData.Cells = new List<CellModel>();
                            var cells = r.Elements<Cell>().ToList();
                            foreach (var h in header.Cells)
                            {
                                string cellReference = Regex.Replace(h.CellReference, @"\d", "") + r.RowIndex;
                                var cell = cells.FirstOrDefault(x => x.CellReference.Value == cellReference);
                                rowData.Cells.Add(new CellModel
                                {
                                    Name = h.Name,
                                    Value = GetCellValue(spreadsheetDocument, cell),
                                    CellReference = cellReference,
                                    DisplayOrder = cell == null ? header.Cells.IndexOf(h) : cells.IndexOf(cell)
                                });
                            }
                            rowsData.Add(rowData);
                        }
                    }
                    catch (Exception e)
                    {
                        Log.Instance.Error($"Errors occur while getting values in file -  row:{r.RowIndex}", e);
                    }
                }
            }

            return true;

        }

        private string GetCellValue(SpreadsheetDocument doc, Cell cell)
        {
            if (cell == null || cell.CellValue == null)
            {
                return string.Empty;
            }

            string value = cell.CellValue.InnerText.Trim();
            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(int.Parse(value)).InnerText.Trim();
            }

            return value;
        }

    }
}