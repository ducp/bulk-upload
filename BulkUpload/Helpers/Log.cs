﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BulkUpload.Helpers
{
    public class Log
    {
        private static ILog _instance = null;

        public static ILog Instance => GetInstance();

        private static ILog GetInstance()
        {
            if (_instance == null)
            {
                _instance = LogManager.GetLogger("");
            }
            return _instance;
        }
    }
}