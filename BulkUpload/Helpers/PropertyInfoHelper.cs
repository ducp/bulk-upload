﻿using BulkUpload.Entities;
using BulkUpload.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace BulkUpload.Helpers
{
    public static class PropertyInfoHelper
    {
        public static bool HasBreezeColumnNameAttribute(this PropertyInfo pi)
        {
            return pi.GetCustomAttributes(typeof(BreezeColumnName), false).Length > 0;
        }

        public static string GetBreezeColumnNameAttributeValue(this PropertyInfo pi)
        {
            return pi.GetCustomAttributes(typeof(BreezeColumnName), false).Cast<BreezeColumnName>().FirstOrDefault()?.Name ?? string.Empty;
        }

        public static bool IsMandatory(this PropertyInfo pi)
        {
            return ((BreezeMandatory[])pi.GetCustomAttributes(typeof(BreezeMandatory), false)).Any();
        }

        public static string GetExportHeaderNameAttributeValue(this PropertyInfo pi)
        {
            return pi.GetCustomAttributes(typeof(ExportHeaderName), false).Cast<ExportHeaderName>().FirstOrDefault()?.Name ?? string.Empty;
        }

        public static LoanData BindClientDataPropertiesToLoanData(this List<LoanProp> loanProps)
        {
            LoanData loanData = new LoanData();
            foreach (var prop in typeof(LoanData).GetProperties().Where(x => x.HasBreezeColumnNameAttribute()))
            {
                var stringValue = loanProps.FirstOrDefault(x => x.BreezePropName.ToLower().Equals(prop.Name.ToLower()))?.StringValue ?? string.Empty;
                if (!string.IsNullOrEmpty(stringValue))
                {
                    var propType = prop.PropertyType;
                    if (propType == typeof(DateTime))
                    {
                        prop.SetValue(loanData, Utils.ConvertFromDateString(stringValue));
                    }
                    else
                    {
                        TypeConverter converter = TypeDescriptor.GetConverter(propType);
                        if (converter.IsValid(stringValue))
                        {
                            prop.SetValue(loanData, converter.ConvertFromString(stringValue));
                        }
                    }
                }
            }
            return loanData;
        }

        public static Dictionary<string, string> UpperKeysInDictionary(this Dictionary<string, string> dict)
        {
            var newDictionary = new Dictionary<string, string>();
            foreach (var kv in dict)
            {
                newDictionary[kv.Key.ToUpper()] = kv.Value;
            }
            return newDictionary;
        }

        public static int GetStringLength(this PropertyInfo pi)
        {
            if (pi.PropertyType == typeof(System.String))
            {
                var attr = (StringLengthAttribute)pi.GetCustomAttribute(typeof(StringLengthAttribute), false);
                if (attr != null)
                {
                    return attr.MaximumLength;
                }
            }
            return 0;
        }

        public static int GetFieldDisplayOrder(this PropertyInfo pi)
        {
            var attr = (FieldOrderAttribute)pi.GetCustomAttribute(typeof(FieldOrderAttribute), false);
            if (attr != null)
            {
                return attr.Order;
            }
            return 0;
        }
    }
}