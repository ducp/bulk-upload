﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Web;

namespace BulkUpload.Helpers
{
    public class ConfigurationProvider
    {
        public static Dictionary<string, Type> SupportedDataTypes = new Dictionary<string, Type>
        {
            { "STRING", typeof(string)  },
            { "INTEGER", typeof(int)  },
            { "BOOLEAN", typeof(bool)  },
            { "DATETIME", typeof(DateTime) },
            { "DECIMAL", typeof(decimal) },
            { "ENUMERATIONS", typeof(Enum) }
        };

        private static Dictionary<SupportedConfiguration, string> dict = new Dictionary<SupportedConfiguration, string>();

        private ConfigurationProvider() { }

        public static T GetValue<T>(SupportedConfiguration config)
        {
            if (dict.ContainsKey(config))
            {
                return (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromString(dict[config]);
            }

            string value = ConfigurationManager.AppSettings[config.ToString()].ToString();
            dict[config] = value;

            return (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromString(value);

        }
    }

    public enum SupportedConfiguration
    {
        TemplatesLocation,
        TempLocation,
        ImportFilesLocation,
        BreezePropertiesFileLocation,
        EpicWebServicesUrl,
        SOAPTemplatesLocation,
        EpicAppLoginSOAPTemplateFilename,
        CookieExpiredInMinutes,
        ExportedXLSXRegLoanDataFilesLocation,
        ExportedErrorLoansFilesLocation,
        PreviewLoansFilesLocation,
        EpicDB
    }

    public enum UndeletableProperties
    {
        ClientLoanNumber,
        Address,
        BorrowerFirstName,
        BorrowerLastName,
        CommitmentNumber
    }
}