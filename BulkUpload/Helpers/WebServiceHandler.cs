﻿using System.IO;
using System.Net;
using System.Xml;

namespace BulkUpload.Helpers
{
    public abstract class WebServiceHandler
    {
        public virtual XmlDocument InvokeService()
        {
            // create SOAP request
            var request = CreateRequest();
            // append body
            BuildSOAPRequestBody(ref request);
            // invoke service
            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader stream = new StreamReader(response.GetResponseStream()))
                {
                    var responseContent = stream.ReadToEnd();
                    var result = new XmlDocument();
                    result.LoadXml(responseContent);
                    return result;
                }
            }
        }

        protected abstract HttpWebRequest CreateRequest();

        protected abstract void BuildSOAPRequestBody(ref HttpWebRequest request);

    }

    public class EpicAppLogin : WebServiceHandler
    {
        private readonly string _soapLoginTemplatePath;
        private readonly string _username;
        private readonly string _password;
        private readonly string _epicWebServicesUrl;

        public EpicAppLogin(string epicWebServicesUrl, string soapLoginTemplatePath, string username, string password)
        {
            _epicWebServicesUrl = epicWebServicesUrl;
            _soapLoginTemplatePath = soapLoginTemplatePath;
            _username = username;
            _password = password;
        }

        protected override HttpWebRequest CreateRequest()
        {
            // SOAP request headers and action
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create($"{_epicWebServicesUrl}?op=AppLogin");
            request.Headers.Add(@"SOAPAction:http://Integra-online.com/AppLogin");
            request.ContentType = "text/xml;charset=\"utf-8\"";
            request.Accept = "text/xml";
            request.Method = "POST";

            return request;
        }

        protected override void BuildSOAPRequestBody(ref HttpWebRequest request)
        {
            XmlDocument xmlBody = new XmlDocument();
            // load request content from template
            xmlBody.Load(_soapLoginTemplatePath);

            var soapBody = xmlBody.GetElementsByTagName("soap:Body")[0];
            if (soapBody != null)
            {
                var loginElm = xmlBody.CreateElement("AppLogin");
                loginElm.SetAttribute("xmlns", "http://Integra-online.com/");
                soapBody.AppendChild(loginElm);

                // append pMethod node
                XmlElement methodElm = xmlBody.CreateElement("pLoginMethod");
                methodElm.InnerText = "Standard";
                loginElm.AppendChild(methodElm);

                // append pFields
                XmlElement pFieldsElm = xmlBody.CreateElement("pLoginFields");
                loginElm.AppendChild(pFieldsElm);

                // append pField
                XmlElement usernameElm = xmlBody.CreateElement("anyType");
                var usernameXsiAttr = xmlBody.CreateAttribute("xsi:type", "http://www.w3.org/2001/XMLSchema-instance");
                usernameXsiAttr.Value = "xsd:string";
                usernameElm.Attributes.Append(usernameXsiAttr);
                usernameElm.InnerText = _username;
                pFieldsElm.AppendChild(usernameElm);

                XmlElement passwordElm = xmlBody.CreateElement("anyType");
                var passwordXsiAttr = xmlBody.CreateAttribute("xsi:type", "http://www.w3.org/2001/XMLSchema-instance");
                passwordXsiAttr.Value = "xsd:string";
                passwordElm.Attributes.Append(passwordXsiAttr);
                passwordElm.InnerText = _password;
                pFieldsElm.AppendChild(passwordElm);

            }

            using (Stream stream = request.GetRequestStream())
            {
                xmlBody.Save(stream);
            }

        }
    }
}