﻿using BulkUpload.Entities;
using BulkUpload.Models;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Unity;

namespace BulkUpload.Helpers
{
    public abstract class FileHandler
    {
        private readonly Dictionary<string, Type> _readers = new Dictionary<string, Type>
        {
            { ".xlsx", typeof(ExcelReader)},
            { ".xls", typeof(ExcelReader)},
            { ".xlsm", typeof(ExcelReader)}
        };

        protected abstract string GetFileExtension();

        protected abstract Stream ReadStream();

        public virtual void ExtractData(ref RowModel headers, ref List<RowModel> rowsData, bool onlyHeader = false)
        {
            var extension = GetFileExtension();

            IUnityContainer container = new UnityContainer();
            var type = _readers[extension];
            FileReader reader = (FileReader)container.Resolve(type, extension);

            using (var stream = ReadStream())
            {
                reader.ReadFileStream(stream, ref headers, ref rowsData, onlyHeader);
            }
        }
    }

    public class HttpPostedFileHandler : FileHandler
    {
        protected override string GetFileExtension()
        {
            var file = HttpContext.Current.Request.Files[0];
            return Path.GetExtension(file.FileName);
        }

        protected override Stream ReadStream()
        {
            var file = HttpContext.Current.Request.Files[0];
            return file.InputStream;
        }
    }

    public class SystemFileHandler : FileHandler
    {
        private readonly string _filePath;
        public SystemFileHandler(string filePath)
        {
            _filePath = filePath;
        }

        protected override string GetFileExtension()
        {
            return Path.GetExtension(_filePath);
        }

        protected override Stream ReadStream()
        {
            return new FileStream(_filePath, FileMode.Open, FileAccess.Read);
        }
    }

    
}