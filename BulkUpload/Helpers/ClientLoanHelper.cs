﻿using BulkUpload.Entities;
using BulkUpload.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BulkUpload.Helpers
{
    public static class ClientLoanHelper
    {
        public static string GetClientLoanNumberFromProperties(this List<LoanProperty> loanProps)
        {
            return loanProps.FirstOrDefault(x => x.PropertyName.Equals(UndeletableProperties.ClientLoanNumber.ToString()))?.Value ?? string.Empty;
        }

        public static string GetClientLoanNumberFromProperties(this List<LoanProp> loanProps)
        {
            return loanProps.FirstOrDefault(x => x.BreezePropName.Equals(UndeletableProperties.ClientLoanNumber.ToString()))?.StringValue ?? string.Empty;
        }

        public static string GetPropertyAddressFromProperties(this List<LoanProperty> loanProps)
        {
            return loanProps.FirstOrDefault(x => x.PropertyName.Equals(UndeletableProperties.Address.ToString()))?.Value ?? string.Empty;
        }

        public static string GetPropertyAddressFromProperties(this List<LoanProp> loanProps)
        {
            return loanProps.FirstOrDefault(x => x.BreezePropName.Equals(UndeletableProperties.Address.ToString()))?.StringValue ?? string.Empty;
        }
    }
}