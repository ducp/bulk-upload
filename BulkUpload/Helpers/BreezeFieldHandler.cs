﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace BulkUpload.Helpers
{
    public class BreezeValidationResult
    {
        public bool Valid { get; set; }
        public string ErrorMessage { get; set; }
    }

    public abstract class BreezeFieldHandler
    {
        protected string _stringValue;
        public BreezeFieldHandler(string stringValue)
        {
            _stringValue = stringValue;
        }

        public abstract BreezeValidationResult Validate();

        public virtual string GetStringValue()
        {
            if (string.IsNullOrEmpty(_stringValue))
            {
                return string.Empty;
            }

            if (!Validate().Valid)
            {
                return _stringValue;
            }

            return FormatStringValue();
        }

        protected virtual string FormatStringValue()
        {
            return _stringValue;
        }

        public virtual string GetDisplayString()
        {
            if (string.IsNullOrEmpty(_stringValue))
            {
                return string.Empty;
            }

            if (!Validate().Valid)
            {
                return _stringValue;
            }

            return FormatDisplayString();
        }

        protected virtual string FormatDisplayString()
        {
            return _stringValue;
        }
    }

    public class StringTypeHandler : BreezeFieldHandler
    {
        private readonly int? _maxStringLength;
        private readonly string _regrex;

        public StringTypeHandler(string stringValue, int? maxStringLength, string regrex) : base(stringValue)
        {
            _maxStringLength = maxStringLength;
            _regrex = regrex;
        }

        public override BreezeValidationResult Validate()
        {
            try
            {
                if (_maxStringLength.HasValue && _stringValue.Length > _maxStringLength.Value)
                {
                    throw new Exception($"Data should not exceed {_maxStringLength.Value} characters");
                }

                if (!string.IsNullOrEmpty(_regrex) && !new Regex(_regrex).IsMatch(_stringValue))
                {
                    throw new Exception("Invalid data");
                }

                return new BreezeValidationResult
                {
                    Valid = true,
                };
            }
            catch (Exception e)
            {
                return new BreezeValidationResult
                {
                    Valid = false,
                    ErrorMessage = e.Message
                };
            }
        }
    }

    public class EnumTypeHandler : BreezeFieldHandler
    {
        private readonly List<string> _options;

        public EnumTypeHandler(string stringValue, List<string> options) : base(stringValue)
        {
            _options = options;
        }

        public override BreezeValidationResult Validate()
        {
            if (!_options.Any(x => x.ToLower().Equals(_stringValue.ToLower())))
            {
                return new BreezeValidationResult
                {
                    Valid = false,
                    ErrorMessage = "Invalid enumerations option"
                };
            }

            return new BreezeValidationResult
            {
                Valid = true,
            };
        }
    }

    public class PrimitiveTypeHandler : BreezeFieldHandler
    {
        private readonly Type _type;
        private readonly bool _mandatory;
        private readonly bool _displayAsCurrency;
        private readonly bool _roundNumberDown;
        private readonly CultureInfo _cultureInfo;

        public PrimitiveTypeHandler(string stringValue, Type type, bool mandatory, bool displayAsCurrency, bool roundNumberDown) : base(stringValue)
        {
            _type = type;
            _mandatory = mandatory;
            _displayAsCurrency = displayAsCurrency;
            _roundNumberDown = roundNumberDown;
            _cultureInfo = CultureInfo.CreateSpecificCulture("en-US");
            _cultureInfo.NumberFormat.CurrencyNegativePattern = 1;
        }

        public override BreezeValidationResult Validate()
        {
            try
            {
                var validString = TypeDescriptor.GetConverter(_type).IsValid(_stringValue);
                if (!validString)
                {
                    throw new Exception("Invalid data");
                }

                if (_mandatory)
                {
                    if (_type == typeof(decimal))
                    {
                        var parsedValue = double.Parse(_stringValue);
                        if (parsedValue == 0)
                        {
                            throw new Exception("Invalid data");
                        }
                    }
                    else if (_type == typeof(int))
                    {
                        var parsedValue = int.Parse(_stringValue);
                        if (parsedValue == 0)
                        {
                            throw new Exception("Invalid data");
                        }
                    }
                }

                return new BreezeValidationResult
                {
                    Valid = true,
                };
            }
            catch (Exception e)
            {
                return new BreezeValidationResult
                {
                    Valid = false,
                    ErrorMessage = e.Message
                };
            }
        }

        protected override string FormatStringValue()
        {
            if (_type == typeof(decimal))
            {
                var parsedValue = double.Parse(_stringValue);
                if (_roundNumberDown)
                {
                    return (parsedValue >= 0 ? Math.Floor(parsedValue) : Math.Ceiling(parsedValue)).ToString();
                }
                else
                {
                    return parsedValue.ToString(); 
                }
            }

            return _stringValue;
        }

        protected override string FormatDisplayString()
        {
            if (_type == typeof(decimal))
            {
                var parsedValue = double.Parse(_stringValue);
                if (_displayAsCurrency)
                {
                    if (_roundNumberDown)
                    {
                        parsedValue = parsedValue >= 0 ? Math.Floor(parsedValue) : Math.Ceiling(parsedValue);
                    }
                    return string.Format(_cultureInfo, "{0:C}", parsedValue);
                }

                return parsedValue.ToString();
            }

            return base.FormatDisplayString();
        }
    }

    public class DateTypeHandler : BreezeFieldHandler
    {
        public DateTypeHandler(string stringValue) : base(stringValue)
        { }

        public override BreezeValidationResult Validate()
        {
            DateTime date;
            try
            {
                double dtDouble;
                if (double.TryParse(_stringValue, out dtDouble))
                {
                    date = DateTime.FromOADate(dtDouble);
                    return new BreezeValidationResult
                    {
                        Valid = true,
                    };
                }

                var formatStrings = new string[] { "M/d/yyyy", "MM/dd/yyyy" };
                if (DateTime.TryParseExact(_stringValue, formatStrings, new CultureInfo("en-US"), DateTimeStyles.None, out date))
                {
                    return new BreezeValidationResult
                    {
                        Valid = true,
                    };
                }

                throw new Exception("Invalid date format");
            }
            catch (Exception e)
            {
                Log.Instance.Error(e);
                return new BreezeValidationResult
                {
                    Valid = false,
                    ErrorMessage = e.Message
                };
            }
        }

        protected override string FormatStringValue()
        {
            return ConvertToDateFormat(_stringValue);
        }

        protected override string FormatDisplayString()
        {
            return ConvertToDateFormat(_stringValue);
        }

        private string ConvertToDateFormat(string stringValue)
        {
            DateTime date;

            double dtDouble;
            if (double.TryParse(stringValue, out dtDouble))
            {
                date = DateTime.FromOADate(dtDouble);
                return date.ToString("M/d/yyyy");
            }

            var formatStrings = new string[] { "M/d/yyyy", "MM/dd/yyyy" };
            if (DateTime.TryParseExact(stringValue, formatStrings, new CultureInfo("en-US"), DateTimeStyles.None, out date))
            {
                return date.ToString("M/d/yyyy");
            }

            return stringValue;
        }
    }

}